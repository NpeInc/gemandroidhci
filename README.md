# GEM Android HCI #

### Introduction ###

The GEM Android HCI repo contains an Android project with an app and library module interfacing to GEM series of modules from North Pole Engineering. The library module uses SWIG to wrap the native C code in the hci_host_reference_design repo with JNI, and the Android app mimics the operations available in the CLI app found there.

Communication with the GEM is done through a serial device (path configured in the app), and so a rooted or custom Android device is necessary (either with a USB-serial cable or UART wired in). 

### Building from source ###

#### Dependencies ####

##### Source #####
The hci_host_reference_design (https://bitbucket.org/NpeInc/hci_host_reference_design) repo must be available at the same location as this repo (../hci_host_reference_design from where this file is located) with submodules included, as there is a soft link to pick up the native code from there. 

The same tag should be checked out for this repo and hci_host_reference_design for the JNI (Java-native interface) wrappers to match the native files they call.

###### Clone Gem Android HCI ######
![gemandroidhci](./img/git_clone_gemandroidhci.png)
###### Clone HCI Reference Design ######
![hci_reference_design](./img/git_clone_hci_reference_design.png)
###### Update HCI Reference Design Submodules ######
![submodule update](./img/git_update_submodule_hci_reference_design.png)

##### LibSerialPort #####

LibSerialPort is a minimal, cross platform shared serial library that is compatible with Windows, Linux, MacOS, Android and FreeBSD. It is licensed under the [GNU Lesser General Public License, version 3 or later](https://www.gnu.org/licenses/lgpl-3.0.en.html). For more information on this library is available [here](https://sigrok.org/wiki/Libserialport).

Serial communications are performed by LibSerialPort and pre-compiled .so shared libraries are provided for the 4 Android platforms.

#### Development Environment ####

A gradle build is provided, and the current directory can be opened as an Android Studio project.

#### Using the GEMHCIModule ####

Before use the native library must be loaded with `System.loadLibrary("gemhci");`

Communication with GEM is done through calls to the static methods in `com.npeinc.gemcontrollerinterface.nativewrap.GemHCIController`. All methods here directly map to native functions, and their documentation applies.

The `com.npeinc.gemcontrollerinterface` and `com.npeinc.gemcontrollerinterface.nativewrap` packages contain all definitions needed.

#### Updating JNI native wrapper files ####

All Java files in the com.npeinc.gemandroid.nativetest package are automatically generated from the native .h files by SWIG (http://www.swig.org) version 4.0.2.

After updating the native code you update the auto-generated wrappers by:
* Changing to the `libs/GemHCIModule/src/main/cpp/gemcontrollerinterface/` directory
* Running `./genjava.sh` (or the equivalent Windows commands)

Most native code will be converted to Java without any problems. If any SWIGTYPE_* classes are generated then a Java type could not be found by SWIG and you will need to update the `libs/GemHCIModule/src/main/cpp/gemcontrollerinterface/GemController.i` SWIG interface file. This is where you can also create typemaps between a Java interface and native function pointer if new callbacks are added, or strip name prefixes that are not necessary in Java.

### Running the app ###

#### Custom Android Device ####

See the hci_host_reference_design readme for details of the serial UART port.

#### Rooted Standard Android Device ####

If you are using a rooted Android device it is possible to connect to GEM with a USB-serial cable.

Ensure that all users have read/write permissions to the `/dev/ttyUSBx` file that is created when connected.

#### No GEM ####

For app development without a GEM, #define `NO_GEM_BOARD_TEST` in ` ../hci_host_reference_design/hci_reference_lib/wf_gem_hci_config.h`.

This will mock GEM serial calls to never occur and always succeed. Note that this means no GEM events will be triggered.

#### Android Virtual Machine ####

It is possible to install Android x86 in a Virtual Machine and connect to GEM with a USB-serial cable in the same way at a rooted physical device. 

##### Steps #####

After installing Android on Virtual box, the following steps can be followed to start the sample GEM HCI App on the platform.

1. Plug GEM into PC using USB cable.
2. Open Virtual Box and wait for it to boot.
3. Select GEM in the Virtual Box by selecting FTDI in Devices menu.

 ![select GEM device](./img/select_ftdi_device_in_android.png)

 4. Assign read write and execute priviledges to the USB serial port. This must be done as super user.

 ![chmod](./img/chmod_android.png)

 5. Start up Android Studio and open the GemAndroidHCI project.
 6. Connect to the Virtual Box Android using ADB. Please note that the IP address must be obtained from the Android device and may be different than below.

![connect](./img/android_studio_connect_and_root.png)

7. Build the project and run. 
8. Once the app start on the Android Device click 'Init'. This will send Pings to the GEM until successful. Once complete click the 'Version' button.

![connect](./img/app_connect_and_get_version.png)

9. The App configures the GEM to connect to an Apple or Samsung watch. 
10. Using an Apple watch tap to pair. 
11. Start the workout by clicking 'IN USE'

![connect](./img/app_after_watch_pair.png)


