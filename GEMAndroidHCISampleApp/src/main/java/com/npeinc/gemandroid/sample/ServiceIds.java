package com.npeinc.gemandroid.sample;

public class ServiceIds {
    private int _id;
    private String _name;

    public ServiceIds(int id, String name)
    {
        this._id = id;
        this._name = name;
    }

    public long getId()
    {
        return _id;
    }

    public void setId(int id)
    {
        _id = id;
    }

    public String getName()
    {
        return _name;
    }

    public void setName(String name)
    {
        _name = name;
    }

    //to display object as a string in spinner
    @Override
    public String toString() {
        return _name;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof ServiceIds){
            ServiceIds c = (ServiceIds)obj;
            if(c.getId()==_id ) return true;
        }

        return false;
    }

}
