package com.npeinc.gemandroid.sample;

public class AntDevice {

    private long _antDeviceNumber;
    private int _profileID;

    public AntDevice(int profileID, long antDeviceNumber)
    {
        this._antDeviceNumber = antDeviceNumber;
        this._profileID = profileID;
    }



    public long getDeviceNumber()
    {
        return _antDeviceNumber;
    }

    public void setDeviceNumber(long deviceNumber)
    {
        _antDeviceNumber = deviceNumber;
    }

    public int getProfileID()
    {
        return _profileID;
    }

    public void getProfileID(int profileID)
    {
        _profileID = profileID;
    }

    //to display object as a string in spinner
    @Override
    public String toString() {
        return "ANT-"+_profileID+"-"+_antDeviceNumber;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof AntDevice){
            AntDevice c = (AntDevice)obj;
            if(c.getDeviceNumber()==_antDeviceNumber && c.getProfileID()==_profileID ) return true;
        }

        return false;
    }

}
