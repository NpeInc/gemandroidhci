//  Copyright (c) 2017-2019 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
package com.npeinc.gemandroid.sample;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.Arrays;

public class AntFragment extends Fragment {
    private static final String TAG = "GEM System";
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    //private long mSelectedServiceID;
    private AntDevice mSelectedDevice;
    private long mSelectedServiceID;
    private ArrayList<AntDevice> mDeviceList;
    private ArrayAdapter<AntDevice> mListAdapter;

    private Spinner mProfileSpinner;
    private ListView mListView;
    private Button mScanButton;
    private Button mConnectButton;
    private Button mDisconnectButton;
    private Button mCalibrateButton;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BleRxFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AntFragment newInstance(String param1, String param2) {
        AntFragment fragment = new AntFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ant, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mScanButton = (Button) view.findViewById(R.id.ant_hrm_discovery_start_button);
        mConnectButton = (Button) view.findViewById(R.id.ant_connect_button);
        mDisconnectButton = (Button) view.findViewById(R.id.ant_disconnect_button);
        mCalibrateButton = (Button) view.findViewById(R.id.ant_pwr_calibrate_button);
        mCalibrateButton.setEnabled(false);

        ArrayList<ServiceIds> profileIDs = new ArrayList<>();
        profileIDs.add((new ServiceIds(120, "HRM")));
        profileIDs.add((new ServiceIds(11, "PWR")));

        mProfileSpinner = (Spinner) view.findViewById(R.id.ant_profile_id_spinner);
        ArrayAdapter<ServiceIds> adapter = new ArrayAdapter<ServiceIds>(view.getContext(), android.R.layout.simple_list_item_1, profileIDs);
        mProfileSpinner.setAdapter(adapter);

        mSelectedServiceID = -1;
        mProfileSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                ServiceIds selectedService = (ServiceIds) adapterView.getItemAtPosition(i);
                if(view != null) {
                    Toast.makeText(view.getContext(), "Selected: " + selectedService.getName(), Toast.LENGTH_LONG).show();
                }
                mSelectedServiceID = selectedService.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                mSelectedServiceID = -1;
            }
        });


        mSelectedDevice = null;
        mDeviceList = new ArrayList<>();
        mListView = (ListView) view.findViewById(R.id.device_list);
        mListAdapter = new ArrayAdapter<AntDevice>(view.getContext(), android.R.layout.simple_list_item_1, mDeviceList);
        mListView.setAdapter(mListAdapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                AntDevice selectedDevice = (AntDevice) adapterView.getItemAtPosition(i);
                mSelectedDevice = selectedDevice;
                mConnectButton.setEnabled(true);
                mDisconnectButton.setEnabled(true);

                if(mSelectedDevice.getProfileID() == 11) {
                    mCalibrateButton.setEnabled(true);
                }
                else
                {
                    mCalibrateButton.setEnabled(false);
                }
            }
        });
    }

    public AntDevice getSelectedDevice() {
        return mSelectedDevice;
    }

    public int getSelectedServiceId(){
        return (int)mSelectedServiceID;
    }

    public void AddDiscoveredDevice( int profileId, long deviceNumber)
    {
        boolean exists = false;
        AntDevice device = new AntDevice(profileId, deviceNumber);
        for(AntDevice x : mDeviceList){
            if(x.getDeviceNumber() == device.getDeviceNumber())
            {
                exists = true;
                break;
            }
        }
        if(!exists)
        {
            Log.e("ANT RX", "Add ANT-" + profileId+"-"+deviceNumber);
            mDeviceList.add(device);
            mListAdapter.notifyDataSetChanged();
        }
    }

    public boolean isScanning()
    {
        if(mScanButton.getText().equals("Stop"))
            return true;
        return false;
    }

    public void startScan()
    {
        mScanButton.setText("Stop");
    }

    public void stopScan()
    {
        mScanButton.setText("Start");
    }
}
