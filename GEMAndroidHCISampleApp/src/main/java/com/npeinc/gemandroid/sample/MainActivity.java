//  Copyright (c) 2017-2019 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
package com.npeinc.gemandroid.sample;

import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.npeinc.gemcontrollerinterface.GemResponseCodeParser;
import com.npeinc.gemcontrollerinterface.IGemDataRequestCallback;
import com.npeinc.gemcontrollerinterface.IGemEventCallback;
import com.npeinc.gemcontrollerinterface.nativewrap.GemHCIController;
import com.npeinc.gemcontrollerinterface.nativewrap.GemHCIControllerConstants;
import com.npeinc.gemcontrollerinterface.nativewrap.gem_event_args_t_ant_receiver_device_discovered;
import com.npeinc.gemcontrollerinterface.nativewrap.gem_event_args_t_ant_receiver_heart_rate_data;
import com.npeinc.gemcontrollerinterface.nativewrap.gem_event_args_t_ant_receiver_power_data;
import com.npeinc.gemcontrollerinterface.nativewrap.gem_event_args_t_ant_receiver_power_calibration_response;
import com.npeinc.gemcontrollerinterface.nativewrap.gem_event_t;
import com.npeinc.gemcontrollerinterface.nativewrap.standard_response_t;
import com.npeinc.gemcontrollerinterface.nativewrap.wf_gem_hci_nfc_tag_board_configuration_t;
import com.npeinc.gemcontrollerinterface.nativewrap.wf_gem_hci_bluetooth_battery_service_e;
import com.npeinc.gemcontrollerinterface.nativewrap.wf_gem_hci_bluetooth_vendor_source_id_e;
import com.npeinc.gemcontrollerinterface.nativewrap.wf_gem_hci_gymconnect_fitness_equipment_state_e;
import com.npeinc.gemcontrollerinterface.nativewrap.wf_gem_hci_gymconnect_fitness_equipment_type_e;
import com.npeinc.gemcontrollerinterface.nativewrap.wf_gem_hci_system_gem_module_version_info_t;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class MainActivity extends AppCompatActivity implements IUIDFUUpdate{
    public class ShortRef { public short value; }
    private static final String TAG = "GEM App";
    private FragmentTransaction mFragmentTransaction;
    private FragmentManager mFragmentManager;
    private SystemFragment mSystemFragment = null;
    private BleFragment mBLEFragment = null;
    private BleRxFragment mBLERxFragment = null;
    private AntFragment mANTFragment = null;
    private GymConnectFragment mGymConnectFragment = null;
    private DfuFragment mDfuFragment = null;
    private NFCReaderFragment mNFCFragment = null;


    private static final int MAX_EVENTS_TO_SHOW = 300;

    // Load the NPE GEM HCI library.
    static {
        System.loadLibrary("gemhci");
    }

    private IGemDataRequestCallback mGemDataRequestCallback = new IGemDataRequestCallback() {
        @Override
        public void onGemDataRequest() {
            send_data_to_gem();
        }
    };



    private IGemEventCallback mGemEventCallback = new IGemEventCallback() {
        @Override
        public void onGemEvent(final gem_event_t event) {

            // CALLED ON RX THREAD. MOVE TO ANOTHER THREAD IMMEDIATELY
            mWorkerHandler.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        doProcessGemEvent(event);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    private EditText mPortEditText = null;
    private Button mInitButton = null;
    private Button mSysButton = null;
    private Button mNFCButton = null;
    private Button mBLEButton = null;
    private Button mBLERxButton = null;
    private Button mANTButton = null;
    private Button mGYMButton = null;
    private Button mDFUBUtton = null;
    private View mContentView = null;
    private View mFragButtonsView = null;
    private TextView mStatusTextView = null;
    
    private boolean mInitialised = false;

    private LinkedList<String> mEventStringList = new LinkedList<>();
    private ArrayAdapter<String> mEventStringdapter;

    private Handler mWorkerHandler = null;
    private DFUProcess mDfuProcess = null;


    private String StringifyNPEErrorCode(int npe_error_code) {
        switch(npe_error_code)
        {
            case GemHCIControllerConstants.NPE_GEM_RESPONSE_OK:
            {
                return("OK");
            }
            case GemHCIControllerConstants.NPE_GEM_RESPONSE_TIMEOUT_OUT:
            {
                return("TIMED OUT");
            }
            case GemHCIControllerConstants.NPE_GEM_RESPONSE_RETRIES_EXHAUSTED:
            {
                return("RETRIES EXHAUSTED");
            }
            case GemHCIControllerConstants.NPE_GEM_RESPONSE_UNABLE_TO_WRITE:
            {
                return("WRITE FAIL");
            }
            case GemHCIControllerConstants.NPE_GEM_RESPONSE_SERIAL_NO_COMPORT:
            {
                return("NO SERIAL PORT");
            }
            case GemHCIControllerConstants.NPE_GEM_RESPONSE_SERIAL_OPEN_FAIL:
            {
                return("SERIAL OPEN FAIL");
            }
            case GemHCIControllerConstants.NPE_GEM_RESPONSE_SERIAL_CONFIG_FAIL:
            {
                return("SERIAL CONFIG FAIL");
            }
            case GemHCIControllerConstants.NPE_GEM_RESPONSE_INVALID_PARAMETER:
            {
                return("INVALID PARAM");
            }
            default:
            {
                return("UNKNOWN ERROR");
            } 
        }
    }

    public void UpdateProgress(int progress)
    {
        // This function will put the write on the UI thread.
        logStatus("Progress " + progress);
    }

    public void UpdateTransferType(int transfer_type)
    {
        // This function will put the write on the UI thread.
        logStatus("TX Type " + transfer_type);
    }

    public void UpdateResult(long err_code)
    {
        String result = StringifyNPEErrorCode((int)err_code);

        updateDFUStatus(result);
        SetDfuEnableStatus(true);

        // If our DFU was successful try to connect to HCI.
        // Otherwise a manual reset of the GEM is required.
        if((int)err_code == GemHCIControllerConstants.NPE_GEM_RESPONSE_OK)
        {
            logStatus("Initializing ...");
            mWorkerHandler.post(mInitGemRunnable);
        }
        else
        {
            logStatus("Manual Reset Required ...");
        }

    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        mPortEditText = findViewById(R.id.port_text);
        mInitButton = findViewById(R.id.init_button);
        mSysButton = findViewById(R.id.goto_system_fragment_button);
        mNFCButton = findViewById(R.id.goto_nfc_fragment_button);
        mBLEButton = findViewById(R.id.goto_ble_fragment_button);
        mBLERxButton = findViewById(R.id.goto_ble_rx_fragment_button);
        mANTButton = findViewById(R.id.goto_ant_fragment_button);
        mGYMButton = findViewById(R.id.goto_gymconnect_fragment_button);
        mDFUBUtton = findViewById(R.id.goto_dfu_fragment_button);


        mContentView = findViewById(R.id.content_frame);
        mFragButtonsView = findViewById(R.id.frag_button_frame);

        mEventStringdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, mEventStringList);


        ListView eventsListView = findViewById(R.id.events_list);
        eventsListView.setAdapter(mEventStringdapter);

        mStatusTextView = findViewById(R.id.status_text);

        mInitialised = false;

        updateStatus("Not Initialised", R.color.colorStatus);

        mWorkerHandler = new Handler();
        SetUIButtonsState(false);

        mFragmentManager = getSupportFragmentManager();
        disableCommandButtons();
    }

    @Override
    protected void onDestroy() {
        GemHCIController.shutdown();

        super.onDestroy();
    }
    private void updateFeState(final wf_gem_hci_gymconnect_fitness_equipment_state_e state) {
        m_equipment_state = state;

        if (state == wf_gem_hci_gymconnect_fitness_equipment_state_e.STATE_FINISHED) {
            m_workout_data.reset();
            refreshWorkoutDataDisplay();
        }

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(mGymConnectFragment != null)
                    mGymConnectFragment.SetFEStateText(state.name());
            }
        });
    }


    private void updateStatus(final String statusMessage, final int colorId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "Run on UI Thread");
                mStatusTextView.setBackgroundColor(getResources().getColor(colorId));
                mStatusTextView.setText(statusMessage);
            }
        });
    }

    private void updateDFUStatus(final String statusMessage)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mDfuFragment.SetDfuStatus(statusMessage);
            }
        });
    }

    private void SetDfuEnableStatus(final boolean enabled)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(enabled) {
                    mDfuFragment.Enable();
                    SetUIButtonsState(true);
                }
                else {
                    mDfuFragment.Disable();
                    SetUIButtonsState(false);
                }
            }
        });
    }

    private void SetUIButtonsState(final boolean enabled)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mSysButton.setEnabled(enabled);
                mNFCButton.setEnabled(enabled);
                mBLEButton.setEnabled(enabled);
                mANTButton.setEnabled(enabled);
                mGYMButton.setEnabled(enabled);
                mDFUBUtton.setEnabled(enabled);
                mBLERxButton.setEnabled(enabled);
            }
        });
    }


    public void logStatus(final String statusMessage) {
        Log.d(TAG, statusMessage);

        updateStatus(statusMessage, R.color.colorStatus);
    }

    private void logError(final String statusMessage) {
        Log.e(TAG, statusMessage);

        updateStatus(statusMessage, R.color.colorError);
    }

    private void logCritical(final String errorMessage) {
        logError(errorMessage);

        if (BuildConfig.DEBUG) {
            throw new AssertionError(errorMessage);
        }
    }

    private void setInitButtonEnabled(boolean enabled) {
        Log.v(TAG, "setInitButtonEnabled " + enabled);

        mPortEditText.setEnabled(enabled);
        mInitButton.setEnabled(enabled);

    }

    private final Runnable mEnableInitButtonRunnable = new Runnable() {
        @Override
        public void run() {
            setInitButtonEnabled(true);
            SetUIButtonsState(false);
        }
    };

    private final Runnable mDisableInitButtonRunnable = new Runnable() {
        @Override
        public void run() {
            setInitButtonEnabled(false);
            SetUIButtonsState(true);
        }
    };

    private void disableInitButton() {
        runOnUiThread(mDisableInitButtonRunnable);
    }

    private void enableInitButton() {
        runOnUiThread(mEnableInitButtonRunnable);
    }

    private void setCommandsVisibility(int visibility) {
        mContentView.setVisibility(visibility);
    }

    private final Runnable mDisableCommandButtonsRunnable = new Runnable() {
        @Override
        public void run() {
            Log.v(TAG, "disableCommandButtons");
            setCommandsVisibility(View.INVISIBLE);
        }
    };

    private void disableCommandButtons() {
        runOnUiThread(mDisableCommandButtonsRunnable);
    }

    private final Runnable mEnableCommandButtonsRunnable = new Runnable() {
        @Override
        public void run() {
            Log.v(TAG, "enableCommandButtons");
            setCommandsVisibility(View.VISIBLE);
        }
    };

    private void enableCommandButtons() {
        runOnUiThread(mEnableCommandButtonsRunnable);
    }

    private void initComplete() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mInitialised = true;
                logStatus("Initialise complete");

                enableCommandButtons();
            }
        });
    }

    /** @brief Updates simulation data and sends to GEM. This function can
     * be called in any context, e.g. 1s timer thread and main. All tx call
     * are marshalled to the tx thread.
     *
     */
    private boolean doSendDataToGem()
    {
        //Log.v(TAG, "doSendDataToGem");

        if(m_equipment_state == wf_gem_hci_gymconnect_fitness_equipment_state_e.STATE_IN_USE || 
            m_equipment_state == wf_gem_hci_gymconnect_fitness_equipment_state_e.STATE_PAUSED) {
            synchronized (m_workout_data) {
                //m_workout_data.speed_centi_kph = 2400; // 24 km/hour
                m_workout_data.distance_meters += 7; // 7 m/s
                m_workout_data.enhanced_speed = 24000000; // 24 km/h
                m_workout_data.average_speed = 24000000; // 24 km/hr.
                m_workout_data.power = 300; // Watts
                m_workout_data.average_power = 300; // Watts 
                m_workout_data.average_cadence = 900;  // 90 RPM
                m_workout_data.cadence = 900; // 90 RPM

                GemHCIController.gymconnect_set_workout_data_elapsed_workout_time(m_workout_data.workout_time_in_seconds++);
                GemHCIController.gymconnect_set_workout_data_cumulative_horizontal_distance(m_workout_data.distance_meters);
                GemHCIController.gymconnect_set_workout_data_grade((short) m_workout_data.deci_grade);
                GemHCIController.gymconnect_set_workout_data_cumulative_energy(m_workout_data.kcal_total);
                GemHCIController.gymconnect_set_workout_data_energy_rate(m_workout_data.rate_kcal_per_hour);
                GemHCIController.gymconnect_set_workout_data_enhanced_speed(m_workout_data.enhanced_speed);
                GemHCIController.gymconnect_set_workout_data_average_speed(m_workout_data.average_speed);
                GemHCIController.gymconnect_set_workout_data_power((short)m_workout_data.power);
                GemHCIController.gymconnect_set_workout_data_average_power((short)m_workout_data.average_power);
                GemHCIController.gymconnect_set_workout_data_cadence(m_workout_data.cadence);
                GemHCIController.gymconnect_set_workout_data_average_cadence(m_workout_data.average_cadence);

            }

            refreshWorkoutDataDisplay();

            standard_response_t response = new standard_response_t();
            long err = GemHCIController.send_command_gymconnect_perform_workout_data_update(response);

            if (err == 0) {
                int errorCode = response.getError_code();
                if (errorCode != 0) {
                    logError("Update Workout Data failed: " +
                            GemResponseCodeParser.NPE_GEM_HCI_LIB_GET_GYMCONNECT_WORKOUT_DATA_UPDATE_ERROR_CODE_STR(errorCode));
                }

            } else {
                logError("Did not get a response to data update: " + decodeGemResponse(err));
                return false;
            }

        }

        return true;
    }

    private Runnable mRefreshWorkoutDataDisplayRunnable = new Runnable() {
        @Override
        public void run() {
            if(mGymConnectFragment != null)
                mGymConnectFragment.SetFEDataText(m_workout_data.toString());
           // mFeDataText.setText(m_workout_data.toString());
        }
    };

    private void refreshWorkoutDataDisplay() {
        runOnUiThread(mRefreshWorkoutDataDisplayRunnable);
    }

    private void addEventToList(String eventAsText) {
        Log.i(TAG, "GEM Event: " + eventAsText);

        Log.v(TAG, "addEventToList Initial List size = " + mEventStringList.size());

        // Ensure we don't make the list longer than we want
        while (mEventStringList.size() >= MAX_EVENTS_TO_SHOW) {
            mEventStringList.removeLast();
        }

        Log.v(TAG, "addEventToList After Removes List size = " + mEventStringList.size());

        // Add our new event to the list
        mEventStringList.addFirst(eventAsText);

        Log.v(TAG, "addEventToList After Adds List size = " + mEventStringList.size());

        // Tell the UI to update
        mEventStringdapter.notifyDataSetChanged();
    }

    /**
     * 
     */
    private String GetNFCTagType(short tagType)
    {
        switch(tagType)
        {
            case 0: return "INVALID";
            case 1: return "Apple Watch";
            case 2: return "Samsung";
            case 3: return "Huawei";
            case 4: return "Mifare Ultralight";
            default: return "UNKNOWN";
        }

    }

    public String GetNFCTagDataType(short dataType)
    {
        switch(dataType)
        {
            case 0: return "INVALID";
            case 1: return "UID";
            case 2: return "Bluetooth Device Address";
            case 3: return "URL";
            case 4:
            default: return "UNKNOWN";
        }

    }



    private void doProcessGemEvent(final gem_event_t event) throws InterruptedException {
        String eventAsText = null;

        switch (event.getMessage_class().getType()) {

            case WF_GEM_HCI_MSG_CLASS_BOOTLOADER:
                eventAsText = "UNEXPECTED Bootloader Event: " + event.getEvent_id().getId();
                logCritical(eventAsText);
                break;
            case WF_GEM_HCI_MSG_CLASS_SYSTEM:
                switch (event.getEvent_id().getSystem_event_type()) {
                    case WF_GEM_HCI_EVENT_ID_SYSTEM_POWER_UP:
                        eventAsText = "System: Power Up";
                        break;
                    case WF_GEM_HCI_EVENT_ID_SYSTEM_SHUTDOWN:
                        eventAsText = "System: Shutdown";

                        if(mDfuFragment != null && mDfuFragment.IgnoreBootloaderInitEvent()) {
                            Thread.sleep(1000);
                            eventAsText = "System: Initiate Bootloader";

                            // Close serial port and thread in hci library
                            GemHCIController.shutdown();

                            // HCI Port no longer initialized. Connected to bootloader now.
                            mInitialised = false;

                            updateDFUStatus("Updating ...");

                            // Start DFU process, this is an asychronous function, retusn right away.
                            mDfuProcess = new DFUProcess(this);
                            mDfuProcess.DoSerialUpdate(mPortEditText.getText().toString(), mDfuFragment.GetDfuFilePath());

                        }

                        break;
                    case WF_GEM_HCI_EVENT_ID_SYSTEM_BOOTLOADER_INITIATED: {
                        eventAsText = "System: Initiate Bootloader";
                        if(mDfuFragment!= null && !mDfuFragment.IgnoreBootloaderInitEvent()) {
                            //eventAsText = "System: Initiate Bootloader";

                            // Close serial port and thread in hci library
                            Thread.sleep(500);
                            GemHCIController.shutdown();

                            // HCI Port no longer initialized. Connected to bootloader now.
                            mInitialised = false;

                            updateDFUStatus("Updating ...");

                            // Start DFU process, this is an asychronous function, retusn right away.
                            mDfuProcess = new DFUProcess(this);
                            mDfuProcess.DoSerialUpdate(mPortEditText.getText().toString(), mDfuFragment.GetDfuFilePath());

                        }
                        break;
                    }
                    default:
                        eventAsText = "UNEXPECTED System Event: " + event.getEvent_id().getId();
                        logCritical(eventAsText);
                        break;
                }
                break;
            case WF_GEM_HCI_MSG_CLASS_HARDWARE:
                eventAsText = "UNEXPECTED Hardware Event: " + event.getEvent_id().getId();
                break;
            case WF_GEM_HCI_MSG_CLASS_BT_CONTROL:
                switch (event.getEvent_id().getBt_control_event_type()) {
                    case WF_GEM_HCI_EVENT_ID_BT_CONTROL_ADV_TIMEOUT:
                        eventAsText = "BT Control: Advertising Timed Out";
                        break;
                    case WF_GEM_HCI_EVENT_ID_BT_CONTROL_CENTRAL_CONNECTED_V1:
                        eventAsText = "BT Control: Central Connected (V1)";
                        break;
                    case WF_GEM_HCI_EVENT_ID_BT_CONTROL_CENTRAL_DISCONNECTED:
                        eventAsText = "BT Control: Central Disconnected";
                        break;
                    case WF_GEM_HCI_EVENT_ID_BT_CONTROL_CENTRAL_CONNECTED:
                        eventAsText = "BT Control: Central Connected";
                        short deviceType = event.getArgs().getBt_control_connected().getDevice_type();
                        break;
                    default:
                        eventAsText = "UNEXPECTED BT Control Event: " + event.getEvent_id().getId();
                        logCritical(eventAsText);
                        break;
                }
                break;
            case WF_GEM_HCI_MSG_CLASS_BT_CONFIG:
                eventAsText = "UNEXPECTED BT Config Event: " + event.getEvent_id().getId();
                logCritical(eventAsText);
                break;
            case WF_GEM_HCI_MSG_CLASS_BT_DEVICE_INFO:
                eventAsText = "UNEXPECTED BT Device Info Event: " + event.getEvent_id().getId();
                logCritical(eventAsText);
                break;
            case WF_GEM_HCI_MSG_CLASS_BT_RECEIVER:
                switch (event.getEvent_id().getBle_receiver_event_type()) {
                    case WF_GEM_HCI_EVENT_ID_BT_RECEIVER_DISCOVERY_TIMEOUT:
                        eventAsText = "BT Receiver: Discovery Timed Out";
                        break;
                    case WF_GEM_HCI_EVENT_ID_BT_RECEIVER_DEVICE_CONNECTED:
                        eventAsText = "BT Receiver: Device Connected";
                        break;
                    case WF_GEM_HCI_EVENT_ID_BT_RECEIVER_CONNECT_FAIL:
                        eventAsText = "BT Receiver: Connect Failed";
                        break;
                    case WF_GEM_HCI_EVENT_ID_BT_RECEIVER_DEVICE_DISCONNECTED:
                        eventAsText = "BT Receiver: Device Disconnected";
                        break;
                    case WF_GEM_HCI_EVENT_ID_BT_RECEIVER_DEVICE_DISCOVERED:
                        if(mBLERxFragment != null) {
                            int serviceId = event.getArgs().getBt_receiver_device_discovered().getService_id();
                            short[] btAddress = event.getArgs().getBt_receiver_device_discovered().getBt_address();
                            String localname = event.getArgs().getBt_receiver_device_discovered().getLocal_name();
                            mBLERxFragment.AddDiscoveredDevice(serviceId, btAddress, localname);
                        }
                        eventAsText = "BT Receiver: Device Discovered";
                        break;
                    case WF_GEM_HCI_EVENT_ID_BT_RECEIVER_CONNECTED_DEVICE_SERIAL_NUMBER_UPDATED:
                        eventAsText = "BT Receiver: Connected Device Serial Number Updated";
                        break;
                    case WF_GEM_HCI_EVENT_ID_BT_RECEIVER_CONNECTED_DEVICE_BATTERY_STATUS_UPDATED:
                        eventAsText = "BT Receiver: Connected Device Battery Status Updated";
                        break;
                    case WF_GEM_HCI_EVENT_ID_BT_RECEIVER_CONNECTED_HEART_RATE_MONITOR_DATA:
                        short heartRate = event.getArgs().getBt_receiver_heart_rate_event().getHeart_rate_data().getHr();
                        eventAsText = "BT Receiver: Connected HRM Data Updated: HR: " + heartRate;
                        break;
                    default:
                        eventAsText = "UNEXPECTED BT Receiver Event: " + event.getEvent_id().getId();
                        logCritical(eventAsText);
                        break;
                }
                break;
            case WF_GEM_HCI_MSG_CLASS_ANT_CONROL:
                eventAsText = "UNEXPECTED ANT Control Event: " + event.getEvent_id().getId();
                logCritical(eventAsText);
                break;
            case WF_GEM_HCI_MSG_CLASS_ANT_CONFIG:
                eventAsText = "UNEXPECTED ANT Config Event: " + event.getEvent_id().getId();
                logCritical(eventAsText);
                break;
            case WF_GEM_HCI_MSG_CLASS_ANT_RECEIVER:
                switch (event.getEvent_id().getAnt_receiver_event_type()) {
                    case WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_DEVICE_DISCOVERED_V1:
                        eventAsText = "ANT Receiver: Device Discovered V1";
                        break;
                    case WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_DISCOVERY_TIMEOUT:
                        eventAsText = "ANT Receiver: Discovery Timeout";
                        break;
                    case WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_DEVICE_CONNECTED:
                        eventAsText = "ANT Receiver: Connect Device Connected";
                        break;
                    case WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_CONNECT_DEVICE_TIMEOUT:
                        eventAsText = "ANT Receiver: Connect Device Timeout";
                        break;
                    case WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_DEVICE_DISCONNECTED:
                        eventAsText = "ANT Receiver: Device Disconnected";
                        break;
                    case WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_DEVICE_DISCOVERED:
                        gem_event_args_t_ant_receiver_device_discovered args = event.getArgs().getAnt_receiver_device_discovered();

                        eventAsText = String.format(Locale.US,
                                "ANT Receiver: Device Discovered.\n" +
                                        "\t\tANT+ Profile ID=%d\n" +
                                        "\t\tDevice Number=%d\n" +
                                        "\t\tRSSI=%ddBm",
                                args.getAnt_plus_profile_id(), args.getDevice_number(), args.getRssi());
                        mANTFragment.AddDiscoveredDevice(args.getAnt_plus_profile_id(), args.getDevice_number());
                        break;
                    case WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_CONNECTED_DEVICE_SERIAL_NUMBER_UPDATED:
                        eventAsText = "ANT Receiver: Connected Device Serial Number Updated";
                        break;
                    case WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_CONNECTED_DEVICE_BATTERY_STATUS_UPDATED:
                        eventAsText = "ANT Receiver: Connected Device Battery Status Updated";
                        break;
                    case WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_CONNECTED_FEC_BASIC_RESISTANCE_CHANGE:
                        eventAsText = "ANT Receiver: Connected Device FE-C Basic Resistance Change";
                        break;
                    case WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_CONNECTED_FEC_POWER_CHANGE:
                        eventAsText = "ANT Receiver: Connected Device FE-C Power Change";
                        break;
                    case WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_CONNECTED_FEC_WIND_RESISTANCE_CHANGE:
                        eventAsText = "ANT Receiver: Connected Device FE-C Wind Resistance Change";
                        break;
                    case WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_CONNECTED_FEC_TRACK_RESISTANCE_CHANGE:
                        eventAsText = "ANT Receiver: Connected Device FE-C Track Resistance Change";
                        break;
                    case WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_CONNECTED_HEART_RATE_MONITOR_DATA:

                        gem_event_args_t_ant_receiver_heart_rate_data hrData = event.getArgs().getAnt_receiver_heart_rate_data();
                        eventAsText = "ANT Receiver: Connected HRM Data: " + hrData.getHeart_rate() + "BPM";
                        break;
                    case WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_CONNECTED_BIKE_POWER_DATA:
                        gem_event_args_t_ant_receiver_power_data pwrData = event.getArgs().getAnt_receiver_power_data();
                        eventAsText = "ANT Receiver: Connected PWR Data: " + pwrData.getData().getPower() + "W";
                        break;
                    case WF_GEM_HCI_EVENT_ID_ANT_RECEIVER_CONNECTED_BIKE_POWER_CALIBRATION_RESPONSE:
                        gem_event_args_t_ant_receiver_power_calibration_response pwrCalResp = event.getArgs().getAnt_receiver_power_calibration_response();
                        eventAsText = String.format(Locale.US,
                                "ANT Receiver: PWR Calibration Response.\n" +
                                        "\t\tResponse=%d\n" +
                                        "\t\tAuto Zero Support=%d\n" +
                                        "\t\tAuto Zero Enabled=%d\n" +
                                        "\t\tData=%d\n",
                                pwrCalResp.getCalibration_response(),
                                pwrCalResp.getAuto_zero_support(),
                                pwrCalResp.getAuto_zero_enabled(),
                                pwrCalResp.getCalibration_data());

                        break;
                    default:
                        eventAsText = "UNEXPECTED ANT Receiver Event: " + event.getEvent_id().getId();
                        logCritical(eventAsText);
                        break;
                }
                break;
            case WF_GEM_HCI_MSG_CLASS_NFC_READER:
                switch (event.getEvent_id().getNfc_reader_event_type()) {
                    case WF_GEM_HCI_EVENT_ID_NFC_READER_READ_EVENT:
                    {

                        eventAsText = "NFC Reader: Read Event\n";
                        eventAsText += "Tag Type: " + GetNFCTagType(event.getArgs().getNfc_reader_event_read().getNfc_tag_type());
                        eventAsText += " Tag Data Type: " + GetNFCTagDataType(event.getArgs().getNfc_reader_event_read().getNfc_tag_data_type());
                        eventAsText += " Payload Length: " + event.getArgs().getNfc_reader_event_read().getPayload_length();

                        for(int i = 0; i < event.getArgs().getNfc_reader_event_read().getPayload_length(); i++)
                        {
                            int b = (int)event.getArgs().getNfc_reader_event_read().getPayload()[i];
                            String payloadByte = String.format("%02X", b);
                            eventAsText += " 0x" + payloadByte + ",";
                        }

                        

                        break;
                    }
                    case WF_GEM_HCI_EVENT_ID_NFC_TAG_BOARD_READ_EVENT:
                        eventAsText = "NFC Tag Board: Tag Board Read Event";
                        break;
                    default:
                        eventAsText = "UNEXPECTED NRF Reader Event: " + event.getEvent_id().getId();
                        //logCritical(eventAsText);
                        break;
                }
                break;
            case WF_GEM_HCI_MSG_CLASS_GYM_CONNECT:
                switch (event.getEvent_id().getGym_connect_event_type()) {
                    case WF_GEM_HCI_EVENT_ID_GYM_CONNECT_HEART_RATE_VALUE_RECEIVED:
                        eventAsText = "Gym Connect: Heart Rate Value=" + event.getArgs().getGymconnect_event_heart_rate_value().getHeart_rate() + "BPM";
                        break;
                    case WF_GEM_HCI_EVENT_ID_GYM_CONNECT_CADENCE_VALUE_RECEIVED:
                        eventAsText = "Gym Connect: Cadence Value=" + event.getArgs().getUint16_value() + "RPM";
                        break;
                    case WF_GEM_HCI_EVENT_ID_GYM_CONNECT_CALORIE_DATA_RECEIVED:
                        eventAsText = "Gym Connect: Calorie Value=" + event.getArgs().getGymconnect_event_calories_value().getAccumulated_total_calories() + "Cals total";
                    case WF_GEM_HCI_EVENT_ID_GYM_CONNECT_USER_INFORMATION_RECEIVED:
                        break;
                    case WF_GEM_HCI_EVENT_ID_GYM_CONNECT_REQUEST_SUPPORTED_UPLOAD_ITEM_TYPES_EVENT:
                        break;
                    case WF_GEM_HCI_EVENT_ID_GYM_CONNECT_BEGIN_UPLOAD_EVENT:
                        break;
                    case WF_GEM_HCI_EVENT_ID_GYM_CONNECT_UPLOAD_CHUNK_EVENT:
                        break;
                    case WF_GEM_HCI_EVENT_ID_GYM_CONNECT_FINISH_UPLOAD_EVENT:
                        break;
                    case WF_GEM_HCI_EVENT_ID_GYM_CONNECT_CANCEL_UPLOAD_EVENT:
                        break;
                    case WF_GEM_HCI_EVENT_ID_GYM_CONNECT_REQUEST_UPLOAD_PROCESSING_AND_ISSUES_EVENT:
                        break;
                    case WF_GEM_HCI_EVENT_ID_GYM_CONNECT_EQUIPMENT_CONTROL_EVENT:
                        break;
                    default:
                        eventAsText = "UNEXPECTED Gym Connect Event: " + event.getEvent_id().getId();
                        logCritical(eventAsText);
                        break;
                }
                break;
        }

        if (eventAsText == null) {
            logCritical("Unhandled Event: class=" + event.getMessage_class() +
                    " id=" + event.getEvent_id().getId());
        }

        addEventToList(eventAsText);
    }

    private final Runnable mSendDataToGemRunnable = new Runnable() {
        @Override
        public void run() {
            doSendDataToGem();
        }
    };
    


    private void send_data_to_gem() {
        mWorkerHandler.post(mSendDataToGemRunnable);
    }

    private boolean writeGemConfig() {

        standard_response_t response = new standard_response_t();

        logStatus("Setting Bluetooth Name...");
        long err = GemHCIController.send_command_bluetooth_config_set_device_name("Android GEM3Ref Des", response);
        if (err == 0) {
            int errorCode = response.getError_code();
            if (errorCode == 0) {
                logStatus("Set Bluetooth Name complete");
            } else {
                    logError("Set Bluetooth Name failed: " +
                            GemResponseCodeParser.NPE_GEM_HCI_LIB_GET_BT_CONFIG_SET_DEVICE_NAME_ERROR_CODE_STR(errorCode));
            }
        } else {
            logCritical("Could not Set Bluetooth Name: " + decodeGemResponse(err));
            return false;
        }

        logStatus("Setting Manufacturer Name...");
        err = GemHCIController.send_command_bluetooth_info_set_manufacturer_name("North Pole Engineering", response);
        if (err == 0) {
            int errorCode = response.getError_code();
            if (errorCode == 0) {
                logStatus("Set Manufacturer Name complete");
            } else {
                logError("Set Manufacturer Name failed: " +
                        GemResponseCodeParser.NPE_GEM_HCI_LIB_GET_BT_INFO_SET_MANUFACTURER_NAME_ERROR_CODE_STR(errorCode));
            }
        } else {
            logCritical("Could not Set Manufacturer Name: " + decodeGemResponse(err));
            return false;
        }

        logStatus("Setting Model Number...");
        err = GemHCIController.send_command_bluetooth_info_set_model_number("1", response);
        if (err == 0) {
            int errorCode = response.getError_code();
            if (errorCode == 0) {
                logStatus("Set Model Number complete");
            } else {
                logError("Set Model Number failed: " +
                        GemResponseCodeParser.NPE_GEM_HCI_LIB_GET_BT_INFO_SET_MODEL_NUMBER_ERROR_CODE_STR(errorCode));
            }
        } else {
            logCritical("Could not Set Model Number: " + decodeGemResponse(err));
            return false;
        }

        logStatus("Setting Serial Number...");
        err = GemHCIController.send_command_bluetooth_info_set_serial_number("5678", response);
        if (err == 0) {
            int errorCode = response.getError_code();
            if (errorCode == 0) {
                logStatus("Set Serial Number complete");
            } else {
                logError("Set Serial Number failed: " +
                        GemResponseCodeParser.NPE_GEM_HCI_LIB_GET_BT_INFO_SET_SERIAL_NUMBER_ERROR_CODE_STR(errorCode));
            }
        } else {
            logCritical("Could not Set Serial Number: " + decodeGemResponse(err));
            return false;
        }

        logStatus("Setting Hardware Revision...");
        err = GemHCIController.send_command_bluetooth_info_set_hardware_rev("2", response);
        if (err == 0) {
            int errorCode = response.getError_code();
            if (errorCode == 0) {
                logStatus("Set Hardware Revision complete");
            } else {
                logError("Set Hardware Revision failed: " +
                        GemResponseCodeParser.NPE_GEM_HCI_LIB_GET_BT_INFO_SET_HARDWARE_REVISION_ERROR_CODE_STR(errorCode));
            }
        } else {
            logCritical("Could not Set Hardware Revision: " + decodeGemResponse(err));
            return false;
        }

        logStatus("Setting Firmware Revision...");
        err = GemHCIController.send_command_bluetooth_info_set_firmware_rev("3", response);
        if (err == 0) {
            int errorCode = response.getError_code();
            if (errorCode == 0) {
                logStatus("Set Firmware Revision complete");
            } else {
                logError("Set Firmware Revision failed: " +
                        GemResponseCodeParser.NPE_GEM_HCI_LIB_GET_BT_INFO_SET_FIRMWARE_REVISION_ERROR_CODE_STR(errorCode));
            }
        } else {
            logCritical("Could not Set Firmware Revision: " + decodeGemResponse(err));
            return false;
        }

        logStatus("Setting Battery Service Included...");
        // TODO Update API to take enum
        err = GemHCIController.send_command_bluetooth_info_set_battery_included(
                (short)wf_gem_hci_bluetooth_battery_service_e.WF_GEM_HCI_BLUETOOTH_BATTERY_SERVICE_NOT_INCLUDE.swigValue(), response);
        if (err == 0) {
            int errorCode = response.getError_code();
            if (errorCode == 0) {
                logStatus("Set Battery Service Included complete");
            } else {
                logError("Set Battery Service Included failed: " +
                        GemResponseCodeParser.NPE_GEM_HCI_LIB_GET_BT_INFO_SET_BATTERY_SERVICE_INCLUDED_ERROR_CODE_STR(errorCode));
            }
        } else {
            logCritical("Could not Set Battery Service Included: " + decodeGemResponse(err));
            return false;
        }

        logStatus("Setting PnP ID ...");
        // TODO Update API to take enum
        err = GemHCIController.send_command_bluetooth_info_set_pnp_id(
                (byte) wf_gem_hci_bluetooth_vendor_source_id_e.WF_GEM_HCI_BLUETOOTH_PNP_ID_SIG_COMPANY_ASSIGNED_NUMBER.swigValue(),
                0x0330,
                24,
                2,
                 response);

        if (err == 0) {
            int errorCode = response.getError_code();
            if (errorCode == 0) {
                logStatus("Setting PnP ID complete");
            } else {
                logError("Setting PnP ID failed: " +
                        GemResponseCodeParser.NPE_GEM_HCI_LIB_GET_BT_INFO_SET_PNP_ID_ERROR_CODE_STR(errorCode));
            }
        } else {
            logCritical("Could not Set PnP ID: " + decodeGemResponse(err));
            return false;
        }



        logStatus("Setting Supported Equipment Control Features...");
        err = GemHCIController.send_command_gymconnect_set_supported_equipment_control_features(0, response);
        if (err == 0) {
            int errorCode = response.getError_code();
            if (errorCode == 0) {
                logStatus("Set Supported Equipment Control Features complete");
            } else {
                logError("Set Supported Equipment Control Features failed: " +
                        GemResponseCodeParser.NPE_GEM_HCI_LIB_GET_GYMCONNECT_SET_SUPPORTED_EQUIPMENT_CONTROL_ERROR_CODE_STR(errorCode));
            }
        } else {
            logCritical("Could not Set Supported Equipment Control Features: " + decodeGemResponse(err));
            return false;
        }

        logStatus("Setting ANT Hardware Version...");
        err = GemHCIController.send_command_ant_config_set_hardware_version((short)1, response);
        if (err == 0) {
            int errorCode = response.getError_code();
            if (errorCode == 0) {
                logStatus("Set ANT Hardware Version complete");
            } else {
                logError("Set ANT Hardware Version failed: " +
                        GemResponseCodeParser.NPE_GEM_HCI_LIB_GET_ANT_CONFIG_SET_HARDWARE_VERSION_ERROR_CODE_STR(errorCode));
            }
        } else {
            logCritical("Could not Set ANT Hardware Version: " + decodeGemResponse(err));
            return false;
        }

        logStatus("Setting ANT Model Number...");
        err = GemHCIController.send_command_ant_config_set_model_number(15000, response);
        if (err == 0) {
            int errorCode = response.getError_code();
            if (errorCode == 0) {
                logStatus("Set ANT Model Number complete");
            } else {
                logError("Set ANT Model Number failed: " +
                        GemResponseCodeParser.NPE_GEM_HCI_LIB_GET_ANT_CONFIG_SET_MODEL_NUMBER_ERROR_CODE_STR(errorCode));
            }
        } else {
            logCritical("Could not Set ANT Model Number: " + decodeGemResponse(err));
            return false;
        }

        logStatus("Setting ANT Software Version...");
        err = GemHCIController.send_command_ant_config_set_software_version((short)1, (short)1, response);
        if (err == 0) {
            int errorCode = response.getError_code();
            if (errorCode == 0) {
                logStatus("Set ANT Software Version complete");
            } else {
                logError("Set ANT Software Version failed: " +
                        GemResponseCodeParser.NPE_GEM_HCI_LIB_GET_ANT_CONFIG_SET_SOFTWARE_VERSION_ERROR_CODE_STR(errorCode));
            }
        } else {
            logCritical("Could not Set ANT Software Version: " + decodeGemResponse(err));
            return false;
        }

        logStatus("Setting ANT Serial Number...");
        err = GemHCIController.send_command_ant_config_set_serial_number(1234, response);
        if (err == 0) {
            int errorCode = response.getError_code();
            if (errorCode == 0) {
                logStatus("Set ANT Serial Number complete");
            } else {
                logError("Set ANT Serial Number failed: " +
                        GemResponseCodeParser.NPE_GEM_HCI_LIB_GET_ANT_CONFIG_SET_SERIAL_NUMBER_ERROR_CODE_STR(errorCode));
            }
        } else {
            logCritical("Could not Set ANT Serial Number: " + decodeGemResponse(err));
            return false;
        }

        logStatus("Setting Fitness Equipment Type...");
        err = GemHCIController.send_command_gymconnect_set_fe_type(
                wf_gem_hci_gymconnect_fitness_equipment_type_e.TYPE_BIKE, response);
        if (err == 0) {
            int errorCode = response.getError_code();
            if (errorCode == 0) {
                logStatus("Set Fitness Equipment Type complete");
            } else {
                logError("Set Fitness Equipment Type failed: " +
                        GemResponseCodeParser.NPE_GEM_HCI_LIB_GET_GYMCONNECT_SET_FITNESS_EQUIPMENT_TYPE_ERROR_CODE_STR(errorCode));
            }
        } else {
            logCritical("Could not Set Fitness Equipment Type: " + decodeGemResponse(err));
            return false;
        }

        if(!doGotoState(wf_gem_hci_gymconnect_fitness_equipment_state_e.STATE_IDLE)) {
            return false;
        }

        logStatus("Starting Bluetooth Advertising...");
        err = GemHCIController.send_command_nfc_reader_radio_enable(response);
        if (err == 0) {
            int errorCode = response.getError_code();
            if (errorCode == 0) {
                logStatus("Enable NFC complete");
            } else {
                logError("Enable NFC failed: " +
                        GemResponseCodeParser.NPE_GEM_HCI_LIB_NFC_ENABLE_DISABLE_ERROR_CODE_STR(errorCode));
            }
        } else {
            logCritical("Could not Enable NFC: " + decodeGemResponse(err));
            return false;
        }

// TODO Do hw pins

        // Send initial data
        if(!doSendDataToGem()) {
            return false;
        }

        return true;
    }

    private void waitForPing() {
        int pingCount = 0;

        boolean waitForPingResponse = true;
        while (waitForPingResponse) {
            logStatus(String.format(Locale.US, "GEM ping #%d...", ++pingCount));
            long errorCode = GemHCIController.send_ping();

            if (errorCode == GemHCIControllerConstants.NPE_GEM_RESPONSE_OK) {
                waitForPingResponse = false;
                logStatus("Received ping response");
            } else {
                logError(String.format(Locale.US, "Ping failed (%s)", decodeGemResponse(errorCode)));
            }
        }

    }

    private String decodeGemResponse(long err) {
        switch((int)err)
        {
            case GemHCIControllerConstants.NPE_GEM_RESPONSE_OK:
            {
                return "OK";
            }
            case GemHCIControllerConstants.NPE_GEM_RESPONSE_TIMEOUT_OUT:
            {
                return "Wait for response timed out";
            }
            case GemHCIControllerConstants.NPE_GEM_RESPONSE_RETRIES_EXHAUSTED:
            {
                return "Retries waiting for response exhausted";
            }
            case GemHCIControllerConstants.NPE_GEM_RESPONSE_UNABLE_TO_WRITE:
            {
                return "Unable to write";
            }
            case GemHCIControllerConstants.NPE_GEM_RESPONSE_SERIAL_NO_COMPORT:
            {
                return String.format("Comport %s does not exist", mPortEditText.getText());
            }
            case GemHCIControllerConstants.NPE_GEM_RESPONSE_SERIAL_OPEN_FAIL:
            {
                return String.format("Unable to open port %s", mPortEditText.getText());
            }
            case GemHCIControllerConstants.NPE_GEM_RESPONSE_SERIAL_CONFIG_FAIL:
            {
                return String.format("Port %s found and opened but unable to configure", mPortEditText.getText());
            }
            case GemHCIControllerConstants.NPE_GEM_RESPONSE_INVALID_PARAMETER:
            {
                return "Invalid parameter";
            }
            default:
            {
                return String.valueOf(err);
            }
        }
    }

    private boolean initGem() {
        logStatus("Initialising...");

        long err = GemHCIController.init(mPortEditText.getText().toString(),
                mGemDataRequestCallback, mGemEventCallback);

        if (err != 0) {
            logError("Could not Initialise: " + decodeGemResponse(err));
            return false;
        }

        waitForPing();

        return writeGemConfig();
    }

    private final Runnable mInitGemRunnable = new Runnable() {
        @Override
        public void run() {
            if(mDfuProcess != null)
                mDfuProcess.stopThread();
            if(initGem()) {
                initComplete();
            } else {
                enableInitButton();
            }
        }
    };

    public void onClickInit(View view) {
        if (mInitialised) {
            return;
        }
        disableInitButton();
        mWorkerHandler.post(mInitGemRunnable);
    }

    private void getVersion() {
        logStatus("Getting Version...");

        standard_response_t response = new standard_response_t();
        long result = GemHCIController.send_command_system_get_version(response);

        if (result == 0) {
            int errorCode = response.getError_code();
            if (errorCode == 0) {
                logStatus("Get Version complete");

                wf_gem_hci_system_gem_module_version_info_t versionInfo = response.getArgs().getSystem_version().getGem_version();

                StringBuilder versionStringBuilder = new StringBuilder();
                versionStringBuilder.append(String.format("Vendor ID: 0x%04x\n", versionInfo.getVendor_id()));
                versionStringBuilder.append(String.format("Product ID: 0x%04x\n", versionInfo.getProduct_id()));
                versionStringBuilder.append(String.format(Locale.US, "HW Version ID: %d\n", versionInfo.getHw_version()));
                versionStringBuilder.append(String.format(Locale.US, "Firmware Version: %d.%d.%d\n", versionInfo.getFw_version_major(), versionInfo.getFw_version_minor(), versionInfo.getFw_version_build()));
                versionStringBuilder.append(String.format(Locale.US, "Firmware Simple: %d\n", versionInfo.getFw_version_simple()));
                versionStringBuilder.append(String.format(Locale.US, "Bootloader Version: %d\n", versionInfo.getBl_version_major()));
                versionStringBuilder.append(String.format(Locale.US, "Bootloader Revision:%d\n", versionInfo.getBl_version_revision()));
                versionStringBuilder.append(String.format("Bootloader Vendor: 0x%04x\n", versionInfo.getBl_version_vendor()));
                versionStringBuilder.append(String.format(Locale.US, "Bootloader Device Variant: %d\n", versionInfo.getBl_version_device_variant()));
                
                if(mSystemFragment != null)
                    mSystemFragment.SetVersion(versionStringBuilder.toString());

                //mVersionText.setText(versionStringBuilder.toString());
            } else {
                logStatus("Get Version failed: " + errorCode);
            }
        } else {
            logStatus("Could not Get Version: " + result);
        }
    }

    // Converts a byte array to a short array of size length.
    // Final length is passed into final_length, which could be smaller than the size of the byte[].
    private short[] ConvertByteToShortArray(byte[] arrayToConvert, int length, ShortRef final_length)
    {
        if(arrayToConvert.length > length)
            final_length.value = (short) length;
        else
            final_length.value =(short) arrayToConvert.length;


        short[] shortArray = new short[length];
        for(int i = 0; i < final_length.value; i++)
            shortArray[i] = (short) (arrayToConvert[i] & 0x00FF);
        return shortArray;
    }

    private void setTagBoardConfig() {
        logStatus("Set Tag Board Config ...");

        byte[] iosNDEF = mNFCFragment.GetIOSNDEFRecord();
        byte[] androidNDEF = mNFCFragment.GetAndroidNDEFRecord();
        byte[] uniqueBleName = mNFCFragment.GetUniqueBLEName();


        Log.i(TAG, "ios length " + iosNDEF.length + "android " + androidNDEF.length + " adv " + uniqueBleName.length + " uuid ");


        ShortRef iosNDEFLength = new ShortRef();
        ShortRef androidNDEFLength = new ShortRef();
        ShortRef uniqueBleNameLength = new ShortRef();



        // Need to convert the byte arrays into short arrays
        short[] iosNDEFShort = ConvertByteToShortArray(iosNDEF, GemHCIControllerConstants.WF_GEM_HCI_TAG_BOARD_CONFIG_MAX_NDEF_PAYLOAD_LENGTH, iosNDEFLength);
        short[] androidNDEFShort = ConvertByteToShortArray(androidNDEF, GemHCIControllerConstants.WF_GEM_HCI_TAG_BOARD_CONFIG_MAX_NDEF_PAYLOAD_LENGTH, androidNDEFLength);
        short[] uniqueBleNameShort = ConvertByteToShortArray(uniqueBleName, GemHCIControllerConstants.WF_GEM_HCI_TAG_BOARD_CONFIG_MAX_ADVERTISING_NAME, uniqueBleNameLength);

        // Set flags based on whether or not NDEF records were set for particular OS.
        short flags = 0;
        if(androidNDEF.length != 0)
            flags |= 0x01;
        if(iosNDEF.length != 0)
            flags |= 0x02;

        // Now fill in the config data
        wf_gem_hci_nfc_tag_board_configuration_t config = new wf_gem_hci_nfc_tag_board_configuration_t();


        config.setOs_supported_flags(flags);
        config.setBluetooth_name_length(uniqueBleNameLength.value);
        config.setUnique_bluetooth_advertising_name(uniqueBleNameShort);
        config.setAndroid_ndef_payload_length(androidNDEFLength.value);
        config.setAndroid_ndef_payload_data(androidNDEFShort);
        config.setIos_ndef_payload_length(iosNDEFLength.value);
        config.setIos_ndef_payload_data(iosNDEFShort);

        // Now send the command to the device
        standard_response_t response = new standard_response_t();
        long result =  GemHCIController.send_command_nfc_reader_set_tag_board_configuration(config, response);

        if (result == 0) {
            int errorCode = response.getError_code();
            if (errorCode == 0) {
                logStatus("Set Tag Board Config complete");
            }
            else {
                logStatus("Set Tag Board Config Failed: " + errorCode);
            }
        }
        else {
            logStatus("Could not Set Tag Board Config: " + result);
        }
    }

    private void getTagBoardConfig() {
        logStatus("Getting Version...");

        standard_response_t response = new standard_response_t();
        long result = GemHCIController.send_command_nfc_reader_get_tag_board_configuration(response);

        if (result == 0) {
            int errorCode = response.getError_code();
            if (errorCode == 0) {
                logStatus("Get Tag Board Config Complete");

                wf_gem_hci_nfc_tag_board_configuration_t tagInfo = response.getArgs().getNfc_reader_set_tag_board_config().getConfig();

                if(mNFCFragment != null)
                {
                    Log.i(TAG, "ios length " + tagInfo.getIos_ndef_payload_length() + "android " +tagInfo.getIos_ndef_payload_length());
                    mNFCFragment.SetIOSNDEFRecord(tagInfo.getIos_ndef_payload_data(), tagInfo.getIos_ndef_payload_length());
                    mNFCFragment.SetAndroidNDEFRecord(tagInfo.getAndroid_ndef_payload_data(), tagInfo.getAndroid_ndef_payload_length());
                    mNFCFragment.SetTagBoardUUID(tagInfo.getTag_board_uuid());
                    mNFCFragment.SetUniqueBLEName(tagInfo.getUnique_bluetooth_advertising_name());
                }

            } else {
                logStatus("Get Tag Board Config failed: " + errorCode);
            }
        } else {
            logStatus("Could not Get Tag Board Config: " + result);
        }
    }
    private void enableNFC() {
        logStatus("Enabling NFC...");

        standard_response_t response = new standard_response_t();
        long result = GemHCIController.send_command_nfc_reader_radio_enable(response);

        if (result == 0) {
            int errorCode = response.getError_code();
            if (errorCode == 0) {
                logStatus("NFC Reader Enabled");

                           } else {
                logStatus("NFC Reader Enable Failed: " + errorCode);
            }
        } else {
            logStatus("Could not Enable NFC: " + result);
        }
    }

    private void disableNFC() {
        logStatus("Disabling NFC...");

        standard_response_t response = new standard_response_t();
        long result = GemHCIController.send_command_nfc_reader_radio_disable(response);

        if (result == 0) {
            int errorCode = response.getError_code();
            if (errorCode == 0) {
                logStatus("NFC Reader Disabled");

            } else {
                logStatus("NFC Reader Disable Failed: " + errorCode);
            }
        } else {
            logStatus("Could not Disable NFC: " + result);
        }
    }

    private void getNFCScanPeriod() {
        logStatus("Getting NFC Scan Period...");

        standard_response_t response = new standard_response_t();
        long result = GemHCIController.send_command_nfc_reader_get_scan_period(response);

        if (result == 0) {
            int errorCode = response.getError_code();
            if (errorCode == 0) {
                logStatus("Get Scan Period complete");

                int scan_period = response.getArgs().getNfc_reader_scan_period().getNfc_reader_scan_period();

                if(mNFCFragment != null)
                    mNFCFragment.SetScanPeriod(scan_period);

            } else {
                logStatus("Get Scan Period failed: " + errorCode);
            }
        } else {
            logStatus("Could not Get Scan Period: " + result);
        }
    }

    private void setNFCScanPeriod() {
        logStatus("Setting NFC Scan Period...");
        standard_response_t response = new standard_response_t();
        int scan_period = mNFCFragment.GetScanPeriod();
        long result = GemHCIController.send_command_nfc_reader_set_scan_period(scan_period, response);

        if (result == 0) {
            int errorCode = response.getError_code();
            if (errorCode == 0) {
                logStatus("Set Scan Period Complete");

            } else {
                logStatus("Set Scan Period failed: " + errorCode);
            }
        } else {
            logStatus("Could not Set Scan Period: " + result);
        }
    }

    private void setNFCRadioTestMode() {
        logStatus("Setting NFC Test Mode...");
        standard_response_t response = new standard_response_t();
        short test_mode = mNFCFragment.GetTestMode();
        long result = GemHCIController.send_command_nfc_reader_set_radio_test_mode(test_mode, response);

        if (result == 0) {
            int errorCode = response.getError_code();
            if (errorCode == 0) {
                logStatus("Set Radio Test Mode Complete");

            } else {
                logStatus("Set Radio Test Mode failed: " + errorCode);
            }
        } else {
            logStatus("Could not Set Radio Test Mode: " + result);
        }
    }

    private void tagRadioCommsCheck() {
        logStatus("Setting NFC Test Mode...");
        standard_response_t response = new standard_response_t();
        long result = GemHCIController.send_command_nfc_tag_board_comms_check(response);

        if (result == 0) {
            int errorCode = response.getError_code();
            if (errorCode == 0) {
                logStatus("Radio Comms Check Complete");

            } else {
                logStatus("Radio Comms Check failed: " + errorCode);
            }
        } else {
            logStatus("Could not do Radio Comms Check: " + result);
        }
    }

    private void startAdvertising() {
        logStatus("Starting Advertising...");

        standard_response_t response = new standard_response_t();
        long result = GemHCIController.send_command_bluetooth_control_start_advertising(response);

        if (result == 0) {
            int errorCode = response.getError_code();
            if (errorCode == 0) {
                logStatus("Start Advertising complete");
            } else {
                logStatus("Start Advertising failed: " + errorCode);
            }
        } else {
            logStatus("Could not Start Advertising: " + result);
        }
    }

    private void startBleDiscovery() {
        logStatus("Starting BLE Scanner...");

        standard_response_t response = new standard_response_t();
        int profileID = (int)mBLERxFragment.getSelectedServiceId();

        if(profileID < 0) {
            Toast.makeText(this, "Must Select Profile ID", Toast.LENGTH_LONG).show();
        }
        else {

            // Disable RSSI filter and timeout.
            byte minRssi = 0;   // Disable
            short discoveryTimeout = 0; // Disable
            long result = GemHCIController.send_command_bt_receiver_start_discovery(profileID, minRssi, discoveryTimeout, response);

            if (result == 0) {
                int errorCode = response.getError_code();
                if (errorCode == 0) {
                    mBLERxFragment.startScan();
                    logStatus("Start Bluetooth Discovery");
                } else {
                    logStatus("Start Bluetooth Discovery failed: " + errorCode);
                }
            } else {
                logStatus("Could not Start Bluetooth Discovery: " + result);
            }
        }
    }
    private void stopBleDiscovery() {
        logStatus("Stopping BLE Discovery...");

        standard_response_t response = new standard_response_t();

        int profileID = (int)mBLERxFragment.getSelectedServiceId();

        if(profileID < 0) {
            Toast.makeText(this, "Must Select a Device", Toast.LENGTH_LONG).show();
        }
        else {


            long result = GemHCIController.send_command_bt_receiver_stop_discovery(profileID, response);

            if (result == 0) {
                int errorCode = response.getError_code();
                if (errorCode == 0) {
                    mBLERxFragment.stopScan();
                    logStatus("Stop Bluetooth Discovery");
                } else {
                    logStatus("Stop Bluetooth Discovery failed: " + errorCode);
                }
            } else {
                logStatus("Could not Stop Bluetooth Discovery: " + result);
            }
        }
    }



    private void connectToBleDevice() {
        logStatus("Connect BLE Device...");

        standard_response_t response = new standard_response_t();

        Device device = mBLERxFragment.getSelectedDevice();

        if(device == null) {
            Toast.makeText(this, "Must Select a Device", Toast.LENGTH_LONG).show();
        }
        else {

            String encodedBTAddress =  getEncodedStringFromShortArray(device.getBleAddress());

            byte connectionTimeout = 0;
            long result = GemHCIController.send_command_bt_receiver_connect_device((int)device.getId(), encodedBTAddress, encodedBTAddress.length(), connectionTimeout, response);

            if (result == 0) {
                int errorCode = response.getError_code();
                if (errorCode == 0) {
                    mBLERxFragment.stopScan();
                    logStatus("Connect Bluetooth Device");
                } else {
                    logStatus("Connect Bluetooth Device failed: " + errorCode);
                }
            } else {
                logStatus("Could not Connect Bluetooth Device: " + result);
            }
        }
    }

    private void connectToAntDevice() {
        logStatus("Connect ANT Device...");

        standard_response_t response = new standard_response_t();

        AntDevice device = mANTFragment.getSelectedDevice();

        if(device == null) {
            Toast.makeText(this, "Must Select a Device", Toast.LENGTH_LONG).show();
        }
        else {
            short connectionTimeout = 0; // Disable timeout
            short proximityBin = 0;     // Disable proximity pairing bin
            long result = GemHCIController.send_command_ant_receiver_connect_device(device.getProfileID(), device.getDeviceNumber(), proximityBin, connectionTimeout, response);


            if (result == 0) {
                int errorCode = response.getError_code();
                if (errorCode == 0) {
                    logStatus("Connect ANT Device");
                } else {
                    logStatus("Connect ANT Device failed: " + errorCode);
                }
            } else {
                logStatus("Could not Connect ANT Device: " + result);
            }
        }
    }

    private void disconnectFromAntDevice() {
        logStatus("Disconnect ANT Device...");

        standard_response_t response = new standard_response_t();

        AntDevice device = mANTFragment.getSelectedDevice();

        if(device == null) {
            Toast.makeText(this, "Must Select a Device", Toast.LENGTH_LONG).show();
        }
        else {
            long result = GemHCIController.send_command_ant_receiver_disconnect_device(device.getProfileID(), response);


            if (result == 0) {
                int errorCode = response.getError_code();
                if (errorCode == 0) {
                    logStatus("Disconnect ANT Device");
                } else {
                    logStatus("Disconnect Bluetooth Device failed: " + errorCode);
                }
            } else {
                logStatus("Could not Disconnect ANT Device: " + result);
            }
        }
    }
    private void antPwrCalibrate() {
        logStatus("Calibrate ANT PWR Device...");

        standard_response_t response = new standard_response_t();

        AntDevice device = mANTFragment.getSelectedDevice();

        if(device == null || device.getProfileID() != 11) {
            Toast.makeText(this, "Must Select a PWR Device", Toast.LENGTH_LONG).show();
        }
        else {
            short attempts = 5; // Disable timeout
            long result = GemHCIController.send_command_ant_receiver_request_calibration(device.getProfileID(), attempts, response);


            if (result == 0) {
                int errorCode = response.getError_code();
                if (errorCode == 0) {
                    logStatus("Calibrate ANT Device");
                } else {
                    logStatus("Calibrate ANT Device failed: " + errorCode);
                }
            } else {
                logStatus("Could not Calibrate ANT Device: " + result);
            }
        }
    }



    private String GetBatteryErrorCode(int errorCode)
    {
        switch(errorCode)
        {
            case 0: return "Success";
            case 1: return "Profile Not Supported";
            case 2: return "Specified Sensor Not Connected";
            case 3: return "Data Not Available";
            case 4: return "Request Not supported";
            case 5: return "No Batteries";
            default: return "UNKNOWN";
        }
    }

    private String GetBatteryStatus(short status)
    {
        switch(status)
        {
            case 1: return "New";
            case 2: return "Good";
            case 3: return "Ok";
            case 4: return "Low";
            case 5: return "Critical";
            default: return "UNKNOWN";
        }
    }

    private void antBatteryRequest(){
        logStatus("Request Battery Status from ANT Device ...");

        standard_response_t response = new standard_response_t();

        AntDevice device = mANTFragment.getSelectedDevice();

        if(device == null) {
            Toast.makeText(this, "Must Select a Device", Toast.LENGTH_LONG).show();
        }
        else {
            short index = 0xFF; // Just the default battery, if two pedal system maywant to specify 0 and 1.
            long result = GemHCIController.send_command_ant_receiver_request_battery_status(device.getProfileID(), index, response);


            if (result == 0) {
                int errorCode = response.getError_code();
                if (errorCode == 0) {
                    double battVoltage = response.getArgs().getAnt_receiver_request_battery_status().getData().getBattery_voltage_course() +
                            (double)response.getArgs().getAnt_receiver_request_battery_status().getData().getBattery_voltage_fractional()/256;
                    short batteryStatus = response.getArgs().getAnt_receiver_request_battery_status().getData().getBattery_status();
                    short numBatteries = response.getArgs().getAnt_receiver_request_battery_status().getData().getNumber_of_batteries();

                    String eventAsText = String.format(Locale.US,
                            "Battery Request Success.\n" +
                                    "\t\tBattery Status=%s\n" +
                                    "\t\tBattery Voltage=%fV\n" +
                                    "\t\t#batteries=%d",
                            GetBatteryStatus(batteryStatus), battVoltage, numBatteries);

                    addEventToList(eventAsText);
                } else {
                    String errorCodeString = GetBatteryErrorCode(errorCode);
                    logStatus("Request Battery Result: " + errorCodeString);
                }
            } else {
                logStatus("Could not Request Battery Status" + result);
            }
        }
    }



    private void disconnectFromBleDevice() {
        logStatus("Disconnect BLE Device...");

        standard_response_t response = new standard_response_t();

        Device device = mBLERxFragment.getSelectedDevice();

        if(device == null) {
            Toast.makeText(this, "Must Select a Device", Toast.LENGTH_LONG).show();
        }
        else {
            long result = GemHCIController.send_command_bt_receiver_disconnect_device((int)device.getId(), response);

            if (result == 0) {
                int errorCode = response.getError_code();
                if (errorCode == 0) {
                    mBLERxFragment.stopScan();
                    logStatus("Disconnect Bluetooth Device");
                } else {
                    logStatus("Disconnect Bluetooth Device failed: " + errorCode);
                }
            } else {
                logStatus("Could not Disconnect Bluetooth Device: " + result);
            }
        }
    }

    private void stopAdvertising() {
        logStatus("Stop Advertising...");

        standard_response_t response = new standard_response_t();
        long result = GemHCIController.send_command_bluetooth_control_stop_advertising(response);

        if (result == 0) {
            int errorCode = response.getError_code();
            if (errorCode == 0) {
                logStatus("Stop Advertising complete");
            } else {
                logStatus("Stop Advertising failed: " + errorCode);
            }
        } else {
            logStatus("Could not Stop Advertising: " + result);
        }
    }

    private void doAntHrmDiscoveryStart() {
        logStatus("Starting ANT+ HRM Discovery...");

        int antPlusProfile = mANTFragment.getSelectedServiceId();

        if(antPlusProfile < 0) {
            Toast.makeText(this, "Must Select Profile ID", Toast.LENGTH_LONG).show();
        }
        else {
            short proximityBin = 0;
            short discoveryTimeout = 30;

            standard_response_t response = new standard_response_t();
            long result = GemHCIController.send_command_ant_receiver_start_discovery(
                    antPlusProfile, proximityBin, discoveryTimeout, response);

            if (result == 0) {
                int errorCode = response.getError_code();
                if (errorCode == 0) {
                    mANTFragment.startScan();
                    logStatus("Start ANT+ HRM Discovery complete");
                } else {
                    logError(String.format(Locale.US, "Start ANT+ HRM Discovery failed: %s",
                            GemResponseCodeParser.NPE_GEM_HCI_LIB_GET_GYMCONNECT_SET_FITNESS_EQUIPMENT_TYPE_ERROR_CODE_STR(errorCode)));
                }
            } else {
                logCritical(String.format(Locale.US, "Could not Start ANT+ HRM Discovery: %s", decodeGemResponse(result)));
            }
        }
    }
    private void doAntHrmDiscoveryStop() {
        logStatus("Stopping ANT+ HRM Discovery...");
        int antPlusProfile = mANTFragment.getSelectedServiceId();

        if(antPlusProfile < 0) {
            Toast.makeText(this, "Must Select Profile ID", Toast.LENGTH_LONG).show();
        }
        else {
            standard_response_t response = new standard_response_t();
            long result = GemHCIController.send_command_ant_receiver_stop_discovery(antPlusProfile, response);

            if (result == 0) {
                int errorCode = response.getError_code();
                if (errorCode == 0) {
                    mANTFragment.stopScan();
                    logStatus("Stop ANT+ HRM Discovery complete");
                } else {
                    logError(String.format(Locale.US, "Stop ANT+ HRM Discovery failed: %s",
                            GemResponseCodeParser.NPE_GEM_HCI_LIB_GET_GYMCONNECT_SET_FITNESS_EQUIPMENT_TYPE_ERROR_CODE_STR(errorCode)));
                }
            } else {
                logCritical(String.format(Locale.US, "Could not Stop ANT+ HRM Discovery: %s", decodeGemResponse(result)));
            }
        }
    }

    private final Runnable mGetVersionRunnable = new Runnable() {
        @Override
        public void run() {
            getVersion();

        }
    };
    private final Runnable mSetTagConfigRunnable = new Runnable() {
        @Override
        public void run() {
            setTagBoardConfig();
        }
    };

    private final Runnable mGetTagConfigRunnable = new Runnable() {
        @Override
        public void run() {
            getTagBoardConfig();

        }
    };



    private final Runnable mEnableNFCRunnable = new Runnable() {
        @Override
        public void run() {
            enableNFC();
        }
    };

    private final Runnable mDisableNFCRunnable = new Runnable() {
        @Override
        public void run() {
            disableNFC();
        }
    };

    private final Runnable mGetNFCScanPeriodRunnable = new Runnable() {
        @Override
        public void run() {
            getNFCScanPeriod();
        }
    };

    private final Runnable mSetNFCScanPeriodRunnable = new Runnable() {
        @Override
        public void run() {
            setNFCScanPeriod();
        }
    };

    private final Runnable mSetNFCSetRadioTestModeRunnable = new Runnable() {
        @Override
        public void run() {
            setNFCRadioTestMode();
        }
    };

    private final Runnable mTagRadioCommsCheckRunnable = new Runnable() {
        @Override
        public void run() {
            tagRadioCommsCheck();
        }
    };

    private final Runnable mStartAdvertisingRunnable = new Runnable() {
        @Override
        public void run() {
            startAdvertising();

        }
    };

    private final Runnable mStopAdvertisingRunnable = new Runnable() {
        @Override
        public void run() {
            stopAdvertising();
        }
    };

    private final Runnable mStartBleDiscoveryRunnable = new Runnable() {
        @Override
        public void run() {
            startBleDiscovery();
        }

    };

    private final Runnable mStopBleDiscoveryRunnable = new Runnable() {
        @Override
        public void run() {
            stopBleDiscovery();
        }

    };

    private final Runnable mConnectBleDeviceRunnable = new Runnable() {
        @Override
        public void run() {
            connectToBleDevice();
        }

    };

    private final Runnable mDisconnectBleDeviceRunnable = new Runnable() {
        @Override
        public void run() {
            disconnectFromBleDevice();
        }

    };

    private final Runnable mAntHrmDiscoveryStartRunnable = new Runnable() {
        @Override
        public void run() {
            doAntHrmDiscoveryStart();

        }
    };

    private final Runnable mAntHrmDiscoveryStopRunnable = new Runnable() {
        @Override
        public void run() {
            doAntHrmDiscoveryStop();

        }
    };

    private final Runnable mConnectAntDeviceRunnable = new Runnable() {
        @Override
        public void run() {
            connectToAntDevice();
        }

    };

    private final Runnable mDisconnectAntDeviceRunnable = new Runnable() {
        @Override
        public void run() {
            disconnectFromAntDevice();
        }

    };

    private final Runnable mAntPwrCalibrateRunnable = new Runnable() {
        @Override
        public void run() {
            antPwrCalibrate();
        }

    };

    private final Runnable mAntBatteryRequestRunnable = new Runnable() {
        @Override
        public void run() {
            antBatteryRequest();
        }

    };



    private final Runnable mDfuStartRunnable = new Runnable() {
        @Override
        public void run() {

            standard_response_t response = new standard_response_t();
            short mode = 0;

            SetDfuEnableStatus(false);
            if(GemHCIController.send_command_system_initiate_bootloader(mode, response) != 0)
                SetDfuEnableStatus(true);
        }
    };



    public void onClickGetVersion(View view) {
        logStatus("Get Version Clicked ...");
        mWorkerHandler.post(mGetVersionRunnable);

    }
    public void onClickSetTagBoardConfig(View view) {

        if(mNFCFragment != null) {

            logStatus("Set NDEF Clicked ...");
            mWorkerHandler.post(mSetTagConfigRunnable);
        }
    }

    public void onClickGetTagBoardConfig(View view) {

        if(mNFCFragment != null) {

            logStatus("Get NDEF Clicked ...");
            mWorkerHandler.post(mGetTagConfigRunnable);
        }
    }



    public void onClickGetEnableNFC(View view)
    {

        if(mNFCFragment != null) {
            logStatus("Enable NFC Clicked ...");
            mWorkerHandler.post(mEnableNFCRunnable);
        }
    }

    public void onClickGetDisableNFC(View view)
    {

        if(mNFCFragment != null) {
            logStatus("Disable NFC Clicked ...");
            mWorkerHandler.post(mDisableNFCRunnable);
        }
    }

    public void onClickGetNFCScanPeriod(View view)
    {

        if(mNFCFragment != null) {
            logStatus("Get NFC Scan Period ...");
            mWorkerHandler.post(mGetNFCScanPeriodRunnable);
        }
    }

    public void onClickSetNFCScanPeriod(View view)
    {

        if(mNFCFragment != null) {
            logStatus("Set NFC Scan Period ...");
            mWorkerHandler.post(mSetNFCScanPeriodRunnable);
        }
    }

    public void onClickSetNFCRadioTestMode(View view)
    {

        if(mNFCFragment != null) {
            logStatus("Set NFC Radio Test Mode ...");
            mWorkerHandler.post(mSetNFCSetRadioTestModeRunnable);
        }
    }

    public void onClickTagRadioCommsCheck(View view)
    {
        if(mNFCFragment != null) {
            logStatus("Tag Radio Comms Check ...");
            mWorkerHandler.post(mTagRadioCommsCheckRunnable);
        }
    }






    public void onClickGotoSysFragment(View view)
    {

        mFragmentTransaction = mFragmentManager.beginTransaction();

        if(mSystemFragment == null)
            mSystemFragment = new SystemFragment();
        mFragmentTransaction.replace(R.id.content_frame, mSystemFragment);
        mFragmentTransaction.commit();
    }

    public void onClickGotoNFCFragment(View view)
    {
        mFragmentTransaction = mFragmentManager.beginTransaction();

        if(mNFCFragment == null)
            mNFCFragment = new NFCReaderFragment();
        mFragmentTransaction.replace(R.id.content_frame, mNFCFragment);
        mFragmentTransaction.commit();
    }

    public void onClickGotoANTFragment(View view)
    {
        mFragmentTransaction = mFragmentManager.beginTransaction();

        if(mANTFragment == null)
            mANTFragment = new AntFragment();
        mFragmentTransaction.replace(R.id.content_frame, mANTFragment);
        mFragmentTransaction.commit();
    }

    public void onClickGotoBLEFragment(View view)
    {
        mFragmentTransaction = mFragmentManager.beginTransaction();

        if(mBLEFragment == null)
            mBLEFragment = new BleFragment();
        mFragmentTransaction.replace(R.id.content_frame, mBLEFragment);
        mFragmentTransaction.commit();
    }

    public void onClickGotoBLERxFragment(View view)
    {
        mFragmentTransaction = mFragmentManager.beginTransaction();

        if(mBLERxFragment == null)
            mBLERxFragment = new BleRxFragment();
        mFragmentTransaction.replace(R.id.content_frame, mBLERxFragment);
        mFragmentTransaction.commit();
    }

    public void onClickGotoGymConnectFragment(View view)
    {
        mFragmentTransaction = mFragmentManager.beginTransaction();

        if(mGymConnectFragment == null)
            mGymConnectFragment = new GymConnectFragment();
        mFragmentTransaction.replace(R.id.content_frame, mGymConnectFragment);
        mFragmentTransaction.commit();
    }
    public void onClickGotoDfuFragment(View view)
    {
        mFragmentTransaction = mFragmentManager.beginTransaction();

        if(mDfuFragment == null)
            mDfuFragment = new DfuFragment();
        mFragmentTransaction.replace(R.id.content_frame, mDfuFragment);
        mFragmentTransaction.commit();
    }


    public void onClickStartAdvertising(View view) {
        //disableCommandButtons();

        mWorkerHandler.post(mStartAdvertisingRunnable);

    }

    public void onClickStopAdvertising(View view) {
        //disableCommandButtons();

        mWorkerHandler.post(mStopAdvertisingRunnable);
    }

    public void onClickStartBLEDiscovery(View view) {
        if(mBLERxFragment.isScanning())
            mWorkerHandler.post(mStopBleDiscoveryRunnable);
        else
            mWorkerHandler.post(mStartBleDiscoveryRunnable);
    }
    public void onClickBLEConnect(View view) {
        mWorkerHandler.post(mConnectBleDeviceRunnable);
    }
    public void onClickBLEDisconnect(View view) {
        mWorkerHandler.post(mDisconnectBleDeviceRunnable);
    }


    public void onClickAntHrmDiscoveryStart(View view) {
        //disableCommandButtons();
        if(mANTFragment.isScanning())
            mWorkerHandler.post(mAntHrmDiscoveryStopRunnable);
        else
            mWorkerHandler.post(mAntHrmDiscoveryStartRunnable);
    }
    public void onClickANTConnect(View view) {
        mWorkerHandler.post(mConnectAntDeviceRunnable);
    }
    public void onClickANTDisconnect(View view) {
        mWorkerHandler.post(mDisconnectAntDeviceRunnable);
    }


    public void onClickAntPwrCalibrate(View view) {
        mWorkerHandler.post(mAntPwrCalibrateRunnable);
    }

    public void onClickAntBatteryRequest(View view) {
        mWorkerHandler.post(mAntBatteryRequestRunnable);
    }



    public void onClickDfuProcessStart(View view) {
        mWorkerHandler.post(mDfuStartRunnable);
    }

    private wf_gem_hci_gymconnect_fitness_equipment_state_e m_equipment_state = wf_gem_hci_gymconnect_fitness_equipment_state_e.STATE_UNKNOWN;

    private boolean doGotoState(@NonNull wf_gem_hci_gymconnect_fitness_equipment_state_e state) {
        logStatus(String.format("Going to State [%s]...", state.name()));

        standard_response_t response = new standard_response_t();
        long result = GemHCIController.send_command_gymconnect_set_fe_state(state, response);

        if (result == 0) {
            int errorCode = response.getError_code();
            if (errorCode == 0) {
                updateFeState(state);
                logStatus(String.format("Go To State [%s] complete", state.name()));
            } else {
                logError(String.format(Locale.US, "Go To State [%s] failed: %s", state.name(),
                        GemResponseCodeParser.NPE_GEM_HCI_LIB_GET_GYMCONNECT_SET_FITNESS_EQUIPMENT_TYPE_ERROR_CODE_STR(errorCode)));
            }
        } else {
            logCritical(String.format(Locale.US, "Could not Go To State [%s]: %s", state.name(), decodeGemResponse(result)));
            return false;
        }

        return true;
    }

    private void gotoState(@NonNull final wf_gem_hci_gymconnect_fitness_equipment_state_e state) {
        //disableCommandButtons();

        mWorkerHandler.post(new Runnable() {
            @Override
            public void run() {
                doGotoState(state);

            }
        });

    }

    public void onClickGotoIdle(View view) {
        gotoState(wf_gem_hci_gymconnect_fitness_equipment_state_e.STATE_IDLE);
    }


    public void onClickGotoInUse(View view) {
        gotoState(wf_gem_hci_gymconnect_fitness_equipment_state_e.STATE_IN_USE);
    }

    public void onClickGotoPaused(View view) {
        gotoState(wf_gem_hci_gymconnect_fitness_equipment_state_e.STATE_PAUSED);
    }

    public void onClickGotoFinished(View view) {
        gotoState(wf_gem_hci_gymconnect_fitness_equipment_state_e.STATE_FINISHED);
    }
    public String getEncodedStringFromShortArray(short[] shortArray) {
        byte[] btAddress = new byte[shortArray.length];

        int i = 0;
        for(short x : shortArray) {
            int aByte = (int)(x & 0x00FF);
            btAddress[i++] = (byte)(aByte);
        }

        return android.util.Base64.encodeToString(btAddress, Base64.DEFAULT);
    }


    private static class IndoorBikeWorkoutData {
        int workout_time_in_seconds;
        int speed_centi_kph;
        int deci_grade;
        int kcal_total;
        int rate_kcal_per_hour;
        int distance_meters;
        int cadence;
        int average_cadence;
        int power;
        int average_power;
        int average_speed;
        int enhanced_speed;

        public IndoorBikeWorkoutData() {
            reset();
        }

        @Override
        @NonNull
        public String toString() {
            synchronized (this) {
                return "IndoorBikeWorkoutData:" +
                        "\n\tworkout_time_in_seconds=" + workout_time_in_seconds +
                        "\n\tenhanced_speed=" + enhanced_speed +
                        "\n\tdeci_grade=" + deci_grade +
                        "\n\tkcal_total=" + kcal_total +
                        "\n\trate_kcal_per_hour=" + rate_kcal_per_hour +
                        "\n\tdistance_meters=" + distance_meters;
            }
        }

        public void reset() {
            workout_time_in_seconds = 0;
            speed_centi_kph = 0;
            kcal_total = 0;
            rate_kcal_per_hour = 0;
            distance_meters = 0;
            cadence = 0;
            average_cadence = 0;
            power = 0;
            average_power = 0;
            average_speed = 0;
            enhanced_speed = 0;
        }
    }

    private final IndoorBikeWorkoutData m_workout_data = new IndoorBikeWorkoutData();

    private static final int GRADE_ADJUST = 50;

    public void onClickGradeDecrease(View view) {
        logStatus(String.format(Locale.US, "Decreasing grade by %d...", GRADE_ADJUST));

        m_workout_data.deci_grade -= GRADE_ADJUST;
        refreshWorkoutDataDisplay();

        logStatus(String.format(Locale.US, "Decreased local grade setting to %d", m_workout_data.deci_grade));
    }

    public void onClickGradeIncrease(View view) {
        logStatus(String.format(Locale.US, "Increasing grade by %d...", GRADE_ADJUST));

        m_workout_data.deci_grade += 50;
        refreshWorkoutDataDisplay();

        logStatus(String.format(Locale.US, "Increased local grade setting to %d", m_workout_data.deci_grade));
    }
}
