package com.npeinc.gemandroid.sample;


public class Device {
    private int _id;
    private short[] _bluetoothAddress;
    private String _name;

    public Device(int id, short[] bluetoothAddress, String name)
    {
        this._id = id;
        this._bluetoothAddress = bluetoothAddress;
        this._name = name;
    }



    public long getId()
    {
        return _id;
    }

    public void setId(int id)
    {
        _id = id;
    }

    public short[] getBleAddress()
    {
        return _bluetoothAddress;
    }

    public void setBleAddress(short[] bleAddress)
    {
        _bluetoothAddress = bleAddress;
    }

    public String getName()
    {
        return _name;
    }

    public void setName(String name)
    {
        _name = name;
    }

    //to display object as a string in spinner
    @Override
    public String toString() {
        return _name;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Device){
            Device c = (Device)obj;
            if(c.getBleAddress().equals(_bluetoothAddress) && c.getId()==_id ) return true;
        }

        return false;
    }

}

