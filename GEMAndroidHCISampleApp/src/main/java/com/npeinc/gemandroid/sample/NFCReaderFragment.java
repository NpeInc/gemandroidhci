//  Copyright (c) 2017-2019 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
package com.npeinc.gemandroid.sample;

import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

public class NFCReaderFragment extends Fragment {

    private EditText mAndroidNdefText = null;
    private EditText miOSEditNdefText = null;
    private EditText mUniqueBleName = null;
    private EditText mTargetUUID = null;
    private EditText mScanPeriodText = null;

    private RadioButton enableRadioTestButton = null;
    private int mScanPeriod = 0;



    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_nfc_reader, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        miOSEditNdefText = view.findViewById(R.id.ios_ndef_text);
        mAndroidNdefText = view.findViewById(R.id.android_ndef_text);
        mUniqueBleName = view.findViewById(R.id.unique_ble_name_text);
        mTargetUUID = view.findViewById(R.id.target_uuid_text);
        mScanPeriodText = view.findViewById(R.id.scan_period_text);
        enableRadioTestButton = view.findViewById(R.id.enable_radio_test_mode);
        mScanPeriodText.setText(Integer.toString(mScanPeriod));


        if(miOSEditNdefText != null) {
            InputFilter inputFilter_Hex = new InputFilter() {
                @Override
                public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                    if (source.length() > 44) return "";// max 44chars

                    // Here you can add more controls, e.g. allow only hex chars etc
                    for (int i = start; i < end; i++) {
                        if (!(source.charAt(i) >= '0' && source.charAt(i) <= '9') &&
                                !(source.charAt(i) >= 'a' && source.charAt(i) <= 'f') &&
                                !(source.charAt(i) >= 'A' && source.charAt(i) <= 'F') &&
                                source.charAt(i) != ',' &&
                                !Character.isSpaceChar(source.charAt(i))) {
                            return "";
                        }
                    }

                    return null;
                }
            };
            miOSEditNdefText.setFilters(new InputFilter[]{inputFilter_Hex});
            miOSEditNdefText.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
            mAndroidNdefText.setFilters(new InputFilter[]{inputFilter_Hex});
            mAndroidNdefText.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);

        }
    }
    public byte[] GetIOSNDEFRecord()
    {
        return GetHexBytes(miOSEditNdefText);
    }

    public void SetIOSNDEFRecord(short[] iosNdefRecord, short length)
    {
        SetHexBytes(iosNdefRecord,length, miOSEditNdefText);
    }

    public byte[] GetAndroidNDEFRecord()
    {
        return GetHexBytes(mAndroidNdefText);
    }

    public void SetAndroidNDEFRecord(short[] androidNdefRecord, short length)
    {
        SetHexBytes(androidNdefRecord,length, mAndroidNdefText);
    }

    public byte[] GetUniqueBLEName()
    {
        return mUniqueBleName.getText().toString().getBytes();
    }

    public void SetUniqueBLEName(short[] uniqueBleName)
    {
        StringBuilder versionStringBuilder = new StringBuilder();

        for(int i = 0; i < uniqueBleName.length; i++)
        {
            if(uniqueBleName[i] == 0x00)
                    break;
            versionStringBuilder.append(String.format("%c", (char)uniqueBleName[i]));
        }
        mUniqueBleName.setText(versionStringBuilder.toString());
    }

    public byte[] GetTagBoardUUID()
    {
        return GetHexBytes(mTargetUUID);
    }

    public void SetTagBoardUUID(short[] targetUuid)
    {
        SetHexBytes(targetUuid, (short)targetUuid.length, mTargetUUID);
    }

    public void SetScanPeriod(int scanPeriod)
    {
        mScanPeriod = scanPeriod;
        mScanPeriodText.setText(Integer.toString(mScanPeriod));
    }

    public int GetScanPeriod()
    {
        return Integer.parseInt(mScanPeriodText.getText().toString(),10);
    }

    public short GetTestMode()
    {
        if(enableRadioTestButton.isChecked())
            return 1;
        else
            return 0;

    }


    private void SetHexBytes(short[] array, short length, EditText editText)
    {
        StringBuilder versionStringBuilder = new StringBuilder();

        for(int i = 0; i < length; i++)
        {
            versionStringBuilder.append(String.format("%02x", array[i]));
            if((i+1) != length)
                versionStringBuilder.append(",");

        }
        editText.setText(versionStringBuilder.toString());
    }


    private byte[] GetHexBytes(EditText editText)
    {
        String hexByteString[] = editText.getText().toString().split(",");
        if(hexByteString == null || hexByteString.length == 0)
            return null;

        Log.i("NFC", "Length " + hexByteString.length);


        byte[] hexBytes = new byte[hexByteString.length];

        for(int i = 0; i < hexByteString.length; i++)
        {
            try {
                Log.i("NFC", "HexByte: " + hexByteString[i]);
                hexBytes[i] = (byte)Integer.parseInt(hexByteString[i], 16);
            }
            catch(Exception e)
            {
                Log.i("NFC", "Fail to parse iOS NDEF record");
                return null;
            }
        }
        return hexBytes;
    }



}