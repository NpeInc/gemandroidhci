//  Copyright (c) 2017-2019 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
package com.npeinc.gemandroid.sample;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

public class DfuFragment extends Fragment {
    private static final String TAG = "GEM DFU";
    private TextView mDfuStatusView = null;
    private EditText mDfuFilePathEditText = null;
    private Button mStartButton = null;
    private CheckBox mIgnoreBootloaderInitEvent = null;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dfu, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        mDfuStatusView = view.findViewById(R.id.dfu_status_text);
        mDfuFilePathEditText = view.findViewById(R.id.dfu_file_text);
        mStartButton = view.findViewById(R.id.dfu_start_process);
        mIgnoreBootloaderInitEvent = view.findViewById((R.id.dfu_ignore_bootloader_event_checkbox));
        mDfuStatusView.setText("Status: ");
    }
    public void SetDfuStatus(String dfu_status)
    {
        mDfuStatusView.setText("Status:" + dfu_status);
    }

    public String GetDfuFilePath()
    {
        return mDfuFilePathEditText.getText().toString();
    }

    /** @brief Enable Start button and DFU file path input.
     *
     */
    public void Enable()
    {
        mStartButton.setEnabled(true);
    }

    /** @brief Disable Start buttin and DFY file path.
     *
     */
    public void Disable()
    {
        mStartButton.setEnabled(false);
    }

    public boolean IgnoreBootloaderInitEvent()
    {
       return mIgnoreBootloaderInitEvent.isChecked();
    }


}
