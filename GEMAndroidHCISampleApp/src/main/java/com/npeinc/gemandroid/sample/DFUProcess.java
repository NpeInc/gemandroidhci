//  Copyright (c) 2017-2019 by North Pole Engineering, Inc.  All rights reserved.
//
//  Printed in the United States of America.  Except as permitted under the United States
//  Copyright Act of 1976, no part of this software may be reproduced or distributed in
//  any form or by any means, without the prior written permission of North Pole
//  Engineering, Inc., unless such copying is expressly permitted by federal copyright law.
//
//  Address copying inquires to:
//  North Pole Engineering, Inc.
//  npe@npe-inc.com
//  221 North First St. Ste. 310
//  Minneapolis, Minnesota 55401
//
//  Information contained in this software has been created or obtained by North Pole Engineering,
//  Inc. from sources believed to be reliable.  However, North Pole Engineering, Inc. does not
//  guarantee the accuracy or completeness of the information published herein nor shall
//  North Pole Engineering, Inc. be liable for any errors, omissions, or damages arising
//  from the use of this software.
package com.npeinc.gemandroid.sample;

import android.util.Log;

import com.npeinc.gemcontrollerinterface.IGemDfuProgressCallback;
import com.npeinc.gemcontrollerinterface.IGemDfuTransferTypeCallback;
import com.npeinc.gemcontrollerinterface.nativewrap.GemHCIController;

public class DFUProcess {
    private IUIDFUUpdate mUIUpdate;
    private Thread mThread;

    public DFUProcess(IUIDFUUpdate uiUpdate)
    {
        mUIUpdate = uiUpdate;
    }

    public void DoSerialUpdate(final String port, final String zipPackageName)
    {
        mThread = new Thread(new Runnable() {
            @Override
            public void run() {
                long result = GemHCIController.npe_hci_dfu_start_update(
                        port,
                        zipPackageName,
                        false,
                        mGemDfuProgressCallback,
                        mGemDfuTransferTypeCallback);
                mUIUpdate.UpdateResult(result);

            }
        });
        mThread.start();

    }
    public void stopThread(){
        if(mThread != null){
            Log.i("DFU", "Stopping Thread");

            mThread.interrupt();
            mThread = null;
        }
    }
    private IGemDfuProgressCallback mGemDfuProgressCallback = new IGemDfuProgressCallback() {
        @Override
        public void onGemDfuProgress(final int progress) {
            mUIUpdate.UpdateProgress(progress);
        }
    };

    private IGemDfuTransferTypeCallback mGemDfuTransferTypeCallback = new IGemDfuTransferTypeCallback() {
        @Override
        public void onGemDfuTransferType(final int transfer_type) {
            mUIUpdate.UpdateTransferType(transfer_type);
        }
    };
}
