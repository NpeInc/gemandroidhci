package com.npeinc.gemandroid.sample;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BleRxFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BleRxFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private long mSelectedServiceID;
    private Device mSelectedDevice;
    private ArrayList<Device> mDeviceList;
    private ArrayAdapter<Device> mListAdapter;

    private Spinner mProfileSpinner;
    private ListView mListView;
    private Button mScanButton;
    private Button mConnectButton;
    private Button mDisconnectButton;



    public BleRxFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BleRxFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BleRxFragment newInstance(String param1, String param2) {
        BleRxFragment fragment = new BleRxFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ble_rx, container, false);
    }



    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        mScanButton = (Button) view.findViewById(R.id.scan_start_button);
        mConnectButton = (Button) view.findViewById(R.id.connect_button);
        mDisconnectButton = (Button) view.findViewById(R.id.disconnect_button);

        ArrayList<ServiceIds> serviceIDs = new ArrayList<>();
        serviceIDs.add((new ServiceIds(0x180D, "HRS")));

        Log.e("BLE RX", "ON CREATE");

        mProfileSpinner = (Spinner) view.findViewById(R.id.profile_id_spinner);
        ArrayAdapter<ServiceIds> adapter = new ArrayAdapter<ServiceIds>(view.getContext(), android.R.layout.simple_list_item_1, serviceIDs);
        mProfileSpinner.setAdapter(adapter);

        mSelectedServiceID = -1;
        mProfileSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                ServiceIds selectedService = (ServiceIds) adapterView.getItemAtPosition(i);
                if(view != null) {
                    Toast.makeText(view.getContext(), "Selected: " + selectedService.getName(), Toast.LENGTH_LONG).show();
                }
                mSelectedServiceID = selectedService.getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                mSelectedServiceID = -1;
            }
        });

        mSelectedDevice = null;
        mDeviceList = new ArrayList<>();
        mListView = (ListView) view.findViewById(R.id.device_list);
        mListAdapter = new ArrayAdapter<Device>(view.getContext(), android.R.layout.simple_list_item_1, mDeviceList);
        mListView.setAdapter(mListAdapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Device selectedDevice = (Device) adapterView.getItemAtPosition(i);
                mSelectedDevice = selectedDevice;
                mConnectButton.setEnabled(true);
                mDisconnectButton.setEnabled(true);

            }
        });
    }

    public long getSelectedServiceId(){
        return mSelectedServiceID;
    }

    public Device getSelectedDevice() {
        return mSelectedDevice;
    }

    public void AddDiscoveredDevice(int serviceId, short[] btAddress, String localName)
    {
        if(localName == null || localName == "" || localName.length()==0)
            return;

        boolean exists = false;
        Device device = new Device(serviceId, btAddress, localName);
        for(Device x : mDeviceList){
            if(Arrays.equals(x.getBleAddress(), device.getBleAddress()))
            {
                exists = true;
                break;
            }
        }
        if(!exists)
        {
            Log.e("BLE RX", "Add " + localName);
            mDeviceList.add(device);
            mListAdapter.notifyDataSetChanged();
        }
    }

    public boolean isScanning()
    {
        if(mScanButton.getText().equals("Stop"))
            return true;
        return false;
    }

    public void startScan()
    {
        mScanButton.setText("Stop");
    }

    public void stopScan()
    {
        mScanButton.setText("Start");
    }
}