#!/bin/bash

# SWIG is required: http://www.swig.org/
# Ths will generate the JNI wrapper file and Java implementations for the C interface.

OUT_DIR="../../java/com/npeinc/gemcontrollerinterface/nativewrap"

rm -f $OUT_DIR/*.h $OUT_DIR/*.c

swig -Wall -java -package com.npeinc.gemcontrollerinterface.nativewrap -outdir $OUT_DIR -o gemcontroller_wrap.c GemController.i

