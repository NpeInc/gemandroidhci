%module GemHCIController

%include "stdint.i"
%include "arrays_java.i"
%include "typemaps.i"
%include "enums.swg"

/* Force the generated Java code to use the #defined values rather than making a JNI call */
%javaconst(1);

// SWIG doesn't like the typecast in these constants #include "src/npe_error_code.h"
#define NPE_GEM_RESPONSE_OK                  (0)
#define NPE_GEM_RESPONSE_TIMEOUT_OUT         (1)
#define NPE_GEM_RESPONSE_RETRIES_EXHAUSTED   (2)
#define NPE_GEM_RESPONSE_UNABLE_TO_WRITE     (3)
#define NPE_GEM_RESPONSE_SERIAL_NO_COMPORT   (4)
#define NPE_GEM_RESPONSE_SERIAL_OPEN_FAIL    (5)
#define NPE_GEM_RESPONSE_SERIAL_CONFIG_FAIL  (6)
#define NPE_GEM_RESPONSE_INVALID_PARAMETER   (7)

// Manually reproduced this in GemResponseCodeParser so can use logcat not just printf:
// #include "src/npe_gem_hci_library_response_strings.h"

%{
#include <assert.h>

#include "src/npe_gem_hci_library_interface.h"

// Store the JVM instance we were created in
JavaVM *vm;

// References to the provided Java callbacks
jobject g_IGemDataRequestCallback;
jmethodID g_onGemDataRequestMethod;
jobject g_IGemEventCallback;
jobject g_IGemDfuProgressCallback;
jobject g_IGemDfuTransferTypeCallback;
jclass g_jgem_event_tClass;
jmethodID g_jgem_event_tConstructor;
jmethodID g_jonGemEventMethod;
jmethodID g_jonGemDfuProgressMethod;
jmethodID g_jonGemDfuTransferTypeMethod;

// The library doesn't let as pass a returned parameter with our callback, so find JNI Env pointer
// by attaching thread to JVM.
JNIEnv *getEnv() {
    JavaVMAttachArgs args = {JNI_VERSION_1_2, 0, 0};
    JNIEnv *java;
    (*vm)->AttachCurrentThread(vm, &java, &args);
    return java;
}

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM *java_vm, void __unused *reserved) {
    JNIEnv *env;
    vm = java_vm;

    if ((*java_vm)->GetEnv(vm, (void **) &env, JNI_VERSION_1_6) != JNI_OK) {
        return JNI_ERR; // JNI version not supported.
    }

    // Save a reference to the gem_event_t parameter class and constructor ready to create instances for callback
    jclass jgem_event_tClass = (*env)->FindClass(env, "com/npeinc/gemcontrollerinterface/nativewrap/gem_event_t");
    assert(jgem_event_tClass);
    g_jgem_event_tClass = (*env)->NewGlobalRef(env, jgem_event_tClass);
    assert(g_jgem_event_tClass);
    g_jgem_event_tConstructor = (*env)->GetMethodID(env, g_jgem_event_tClass, "<init>", "(JZ)V" );
    assert(g_jgem_event_tConstructor);

    return JNI_VERSION_1_6;
}

// An implementation of one_second_timeout_t().
// Dispatches a call to the Java IGemDataRequestCallback.onGemDataRequest() using JNI.
void one_second_timeout_callback(void) {
    JNIEnv *env = getEnv();

    // Call IGemDataRequestCallback.onGemDataRequest()
    (*env)->CallVoidMethod(env, g_IGemDataRequestCallback, g_onGemDataRequestMethod);

    if ((*env)->ExceptionCheck(env)) {
        (*env)->ExceptionDescribe(env);
    }

    (*vm)->DetachCurrentThread(vm);
}

// An implementation of on_gem_event_cb_t().
// Converts the native gem_event_t param and dispatches a call to Java
// IGemEventCallback.onGemEvent(gem_event_t) using JNI.
void on_gem_event_callback(gem_event_t* gem_event) {
    JNIEnv *env = getEnv();

    long gem_event_cptr = (long)&gem_event;
    jlong gem_event_cptr_jlong  = (jlong)gem_event;

    // Copy gem_event off the stack so it will persist to Java. 
    gem_event_t *pgem_event_for_java = (gem_event_t *)malloc(sizeof(gem_event_t));
    memcpy(pgem_event_for_java, gem_event, sizeof(gem_event_t));
    jlong address_gem_event_for_java = (jlong)pgem_event_for_java;

    // Construct Java gem_event_t wrapping the native version, telling Java to manage native memory.
    jobject jgem_event_tObject =
            (jobject)(*env)->NewObject(env, g_jgem_event_tClass, g_jgem_event_tConstructor, address_gem_event_for_java, JNI_TRUE);
    assert(jgem_event_tObject);

    // Call IGemEventCallback.onGemEvent()
    (*env)->CallVoidMethod(env, g_IGemEventCallback, g_jonGemEventMethod, jgem_event_tObject);

    if ((*env)->ExceptionCheck(env)) {
        (*env)->ExceptionDescribe(env);
    }

    (*vm)->DetachCurrentThread(vm);
}


// An implementation of dfu_progress_callback_t().
// Converts the native int param and dispatches a call to Java
// IGemDfuEventCallback.onGemDfuProgress(int) using JNI.
void on_gem_dfu_progress_callback(int progress) {
    JNIEnv *env = getEnv();
    
    jint progress_as_java_type = (jint)progress;

    (*env)->CallVoidMethod(env, g_IGemDfuProgressCallback, g_jonGemDfuProgressMethod, progress_as_java_type);

    if ((*env)->ExceptionCheck(env)) {
        (*env)->ExceptionDescribe(env);
    }

    //(*vm)->DetachCurrentThread(vm);
}

// An implementation of dfu_transfer_type_callback_t().
// Converts the native int param and dispatches a call to Java
// IGemDfuEventCallback.onGemDfuProgress(int) using JNI.
void on_gem_dfu_transfer_type_callback(int transfer_type) {
    JNIEnv *env = getEnv();

    jint transfer_type_as_java_type = (jint)transfer_type;

    // Call IGemEventCallback.onGemEvent()
    (*env)->CallVoidMethod(env, g_IGemDfuTransferTypeCallback, g_jonGemDfuTransferTypeMethod, transfer_type_as_java_type);

    if ((*env)->ExceptionCheck(env)) {
        (*env)->ExceptionDescribe(env);
    }

    //(*vm)->DetachCurrentThread(vm);
}


%}

// Could ignore methods/values not used by the app API and only keep those meant to be public.
// Instead, #ifndef SWIG has been manually added to internal method definitions so they are not
// wrapped by SWIG.
//%ignore ""; // ignore all

// UTF-8 Data Type is a String
%apply char * { utf8_data_t* };

// FIXME Make sure this is large enough in Java to hold unsigned where used.
%apply char * { unsigned char * };

%apply jbyteArray { const uint8_t[] };
%apply jbyteArray { uint8_t[] };

// Could rename types and functions to follow standard Java conventions, but then it would be harder
// to find the matching native code.
// Make camelcase
/* (Generic rename overrides all others)
%rename("%(lowercamelcase)s", %$isfunction) ""; // foo_bar -> fooBar; FooBar -> fooBar
%rename("%(camelcase)s", regextarget=1) "_t$"; // foo_bar_t -> FooBar; foo_bar -> foo_bar
%rename("%(camelcase)s", regextarget=1) "_e$";
 */

// Turn on the features we need
%include "src/hci_reference_lib/wf_gem_hci_config.h"

// Get Gym Connect enums we need
// NOT IGNORING ANYTHING %rename("$ignore") ""; // ignore all
//%rename("%s") ""; // ignore none
// NOT IGNORING ANYTHING %rename("$ignore", %$isfunction) ""; // ignore functions
// Except
// wf_gem_hci_gymconnect_fitness_equipment_type_e
// wf_gem_hci_gymconnect_fitness_equipment_state_e
// (Give enums a "class like" name without prefix)
//!!%rename("FitnessEquipmentState") "wf_gem_hci_gymconnect_fitness_equipment_state_e";
//!!%rename("FitnessEquipmentType") "wf_gem_hci_gymconnect_fitness_equipment_type_e";
// Keep the enum elements, but strip the FE Type/Status prefix (which are redundant in C#/Java)
//%rename("%(regex:/WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_([A-Za-z0-9]+)+_(.*)/\\2/)s", %$isenumitem, regextarget=1) "^WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT";
%rename("%(regex:/WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT_(.*)/\\1/)s", %$isenumitem, regextarget=1) "^WF_GEM_HCI_GYMCONNECT_FITNESS_EQUIPMENT";
// Remove unnecessary function prefixes
%rename("%(regex:/wf_gem_hci_manager_(.*)/\\1/)s", %$isfunction, regextarget=1) "^wf_gem_hci_manager_";
%include "src/hci_reference_lib/wf_gem_hci_manager_gymconnect.h"

// Get version info type
// NOT IGNORING ANYTHING %rename("$ignore") ""; // ignore all
// Except
// wf_gem_hci_system_gem_module_version_info_t
// (Give a "class like" name without prefix)
//!!%rename("GemModuleVersionInfo") "wf_gem_hci_system_gem_module_version_info_t";
//!!%rename("BluetoothBatteryServiceInclude") "wf_gem_hci_bluetooth_battery_service_e";
%include "src/hci_reference_lib/wf_gem_hci_manager.h"

// NOT IGNORING ANYTHING %rename("%s") ""; // ignore none
// typemaps that cause the callback method to be extracted from the IGemDataRequestCallback objects
// and save it so the function we pass to the native code will be able to call it.
%typemap(jstype) one_second_timeout_t "com.npeinc.gemcontrollerinterface.IGemDataRequestCallback";
%typemap(jtype) one_second_timeout_t "com.npeinc.gemcontrollerinterface.IGemDataRequestCallback";
%typemap(jni) one_second_timeout_t "jobject";
%typemap(javain) one_second_timeout_t "$javainput";
%typemap(in) one_second_timeout_t {
    // This will be processed when the Java code calls init

    // JCALLx macros from in ib/java/javahead.swg, where x is the number of arguments in the C
    // version of the JNI call.
    JCALL1(GetJavaVM, jenv, &vm);
    g_IGemDataRequestCallback = JCALL1(NewGlobalRef, jenv, $input);
    assert(g_IGemDataRequestCallback);

    // Find IGemDataRequestCallback.onGemDataRequest()
    // Save this to be called by our native callback wrapper
    jclass jGemDataRequestCallbackClass = JCALL1(GetObjectClass, jenv, $input);
    assert(jGemDataRequestCallbackClass);
    g_onGemDataRequestMethod = JCALL3(GetMethodID, jenv, jGemDataRequestCallbackClass, "onGemDataRequest", "()V");
    assert(g_onGemDataRequestMethod);

    JCALL1(DeleteLocalRef, jenv, $input);

    // Pass this as the actual callback reference for the native code to call
    $1 = one_second_timeout_callback;
};

// typemaps that cause the callback method to be extracted from the IGemEventCallback objects and
// save it so the function we pass to the native code will be able to call it.
%typemap(jstype) on_gem_event_cb_t "com.npeinc.gemcontrollerinterface.IGemEventCallback";
%typemap(jtype) on_gem_event_cb_t "com.npeinc.gemcontrollerinterface.IGemEventCallback";
%typemap(jni) on_gem_event_cb_t "jobject";
%typemap(javain) on_gem_event_cb_t "$javainput";
%typemap(in) on_gem_event_cb_t {
    // This will be processed when the Java code calls init

    JCALL1(GetJavaVM, jenv, &vm);
    g_IGemEventCallback = JCALL1(NewGlobalRef, jenv, $input);
    assert(g_IGemEventCallback);

    // Find IGemEventCallback.onGemEvent()
    // Save this to be called by our native callback wrapper
    jclass jGemEventCallbackClass = JCALL1(GetObjectClass, jenv, $input);
    assert(jGemEventCallbackClass);
    g_jonGemEventMethod = JCALL3(GetMethodID, jenv, jGemEventCallbackClass, "onGemEvent", "(Lcom/npeinc/gemcontrollerinterface/nativewrap/gem_event_t;)V");
    assert(g_jonGemEventMethod);

    JCALL1(DeleteLocalRef, jenv, $input);

    // Pass this as the actual callback reference for the native code to call
    $1 = on_gem_event_callback;
};

// typemaps that cause the callback method to be extracted from the IGemEventCallback objects and
// save it so the function we pass to the native code will be able to call it.
%typemap(jstype) dfu_progress_callback_t "com.npeinc.gemcontrollerinterface.IGemDfuProgressCallback";
%typemap(jtype) dfu_progress_callback_t "com.npeinc.gemcontrollerinterface.IGemDfuProgressCallback";
%typemap(jni) dfu_progress_callback_t "jobject";
%typemap(javain) dfu_progress_callback_t "$javainput";
%typemap(in) dfu_progress_callback_t {
    // This will be processed when the Java code calls init

    JCALL1(GetJavaVM, jenv, &vm);
    g_IGemDfuProgressCallback = JCALL1(NewGlobalRef, jenv, $input);
    assert(g_IGemDfuProgressCallback);

    // Find IGemEventCallback.onGemEvent()
    // Save this to be called by our native callback wrapper
    jclass jGemDfuProgressCallbackClass = JCALL1(GetObjectClass, jenv, $input);
    assert(jGemDfuProgressCallbackClass);
    g_jonGemDfuProgressMethod = JCALL3(GetMethodID, jenv, jGemDfuProgressCallbackClass, "onGemDfuProgress", "(I)V");
    assert(g_jonGemDfuProgressMethod);

    JCALL1(DeleteLocalRef, jenv, $input);

    // Pass this as the actual callback reference for the native code to call
    $1 = on_gem_dfu_progress_callback;
};

// typemaps that cause the callback method to be extracted from the IGemEventCallback objects and
// save it so the function we pass to the native code will be able to call it.
%typemap(jstype) dfu_transfer_type_callback_t "com.npeinc.gemcontrollerinterface.IGemDfuTransferTypeCallback";
%typemap(jtype) dfu_transfer_type_callback_t "com.npeinc.gemcontrollerinterface.IGemDfuTransferTypeCallback";
%typemap(jni) dfu_transfer_type_callback_t "jobject";
%typemap(javain) dfu_transfer_type_callback_t "$javainput";
%typemap(in) dfu_transfer_type_callback_t {
    // This will be processed when the Java code calls init

    JCALL1(GetJavaVM, jenv, &vm);
    g_IGemDfuTransferTypeCallback = JCALL1(NewGlobalRef, jenv, $input);
    assert(g_IGemDfuTransferTypeCallback);

    // Find IGemEventCallback.onGemEvent()
    // Save this to be called by our native callback wrapper
    jclass jGemDfuTransferTypeCallbackClass = JCALL1(GetObjectClass, jenv, $input);
    assert(jGemDfuTransferTypeCallbackClass);
    g_jonGemDfuTransferTypeMethod = JCALL3(GetMethodID, jenv, jGemDfuTransferTypeCallbackClass, "onGemDfuTransferType", "(I)V");
    assert(g_jonGemDfuTransferTypeMethod);

    JCALL1(DeleteLocalRef, jenv, $input);

    // Pass this as the actual callback reference for the native code to call
    $1 = on_gem_dfu_transfer_type_callback;
};




// Remove unnecessary function prefixes
%rename("%(regex:/npe_hci_library_(.*)/\\1/)s", %$isfunction, regextarget=1) "^npe_hci_library_"; // npe_hci_library_hello() -> hello()
%rename("%(regex:/npe_gem_hci_library_interface_(.*)/\\1/)s", %$isfunction, regextarget=1) "^npe_gem_hci_library_interface_"; // npe_hci_library_hello() -> hello()

// Give types a "class like" name without prefix
//!!%rename("GemHciPin") "npe_hci_pin_t";
//!!%rename("GemStandardResponse") "standard_response_t";
//!!%rename("GemResponseData") "standard_response_t_args";
//!!%rename("GemHwPins") "standard_response_t_args_hw_get_pins";

%include "src/npe_gem_hci_library_interface.h"
