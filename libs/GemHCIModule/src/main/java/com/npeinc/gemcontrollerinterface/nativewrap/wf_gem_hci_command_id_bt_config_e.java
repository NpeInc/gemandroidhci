/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.2
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.npeinc.gemcontrollerinterface.nativewrap;

public enum wf_gem_hci_command_id_bt_config_e {
  WF_GEM_HCI_COMMAND_ID_BT_CONFIG_GET_DEVICE_NAME(0x01),
  WF_GEM_HCI_COMMAND_ID_BT_CONFIG_SET_DEVICE_NAME(0x02),
  WF_GEM_HCI_COMMAND_ID_BT_CONFIG_GET_ADV_TIMING(0x03),
  WF_GEM_HCI_COMMAND_ID_BT_CONFIG_SET_ADV_TIMING(0x04),
  WF_GEM_HCI_COMMAND_ID_BT_CONFIG_GET_DEVICE_ADDR(0x05),
  WF_GEM_HCI_COMMAND_ID_BT_CONFIG_SET_DEVICE_ADDR(0x06),
  WF_GEM_HCI_COMMAND_ID_BT_CONFIG_GET_RADIO_TX_POWER(0x07),
  WF_GEM_HCI_COMMAND_ID_BT_CONFIG_SET_RADIO_TX_POWER(0x08),
  WF_GEM_HCI_COMMAND_ID_BT_CONFIG_GET_CONNECTION_INTERVAL(0x09),
  WF_GEM_HCI_COMMAND_ID_BT_CONFIG_SET_CONNECTION_INTERVAL(0x0A);

  public final int swigValue() {
    return swigValue;
  }

  public static wf_gem_hci_command_id_bt_config_e swigToEnum(int swigValue) {
    wf_gem_hci_command_id_bt_config_e[] swigValues = wf_gem_hci_command_id_bt_config_e.class.getEnumConstants();
    if (swigValue < swigValues.length && swigValue >= 0 && swigValues[swigValue].swigValue == swigValue)
      return swigValues[swigValue];
    for (wf_gem_hci_command_id_bt_config_e swigEnum : swigValues)
      if (swigEnum.swigValue == swigValue)
        return swigEnum;
    throw new IllegalArgumentException("No enum " + wf_gem_hci_command_id_bt_config_e.class + " with value " + swigValue);
  }

  @SuppressWarnings("unused")
  private wf_gem_hci_command_id_bt_config_e() {
    this.swigValue = SwigNext.next++;
  }

  @SuppressWarnings("unused")
  private wf_gem_hci_command_id_bt_config_e(int swigValue) {
    this.swigValue = swigValue;
    SwigNext.next = swigValue+1;
  }

  @SuppressWarnings("unused")
  private wf_gem_hci_command_id_bt_config_e(wf_gem_hci_command_id_bt_config_e swigEnum) {
    this.swigValue = swigEnum.swigValue;
    SwigNext.next = this.swigValue+1;
  }

  private final int swigValue;

  private static class SwigNext {
    private static int next = 0;
  }
}

