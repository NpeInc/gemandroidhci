/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.2
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.npeinc.gemcontrollerinterface.nativewrap;

public class npe_inc_function_args_bt_device_info_set_pnp_id {
  private transient long swigCPtr;
  protected transient boolean swigCMemOwn;

  protected npe_inc_function_args_bt_device_info_set_pnp_id(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(npe_inc_function_args_bt_device_info_set_pnp_id obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  @SuppressWarnings("deprecation")
  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        GemHCIControllerJNI.delete_npe_inc_function_args_bt_device_info_set_pnp_id(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public void setVendor_source_id(short value) {
    GemHCIControllerJNI.npe_inc_function_args_bt_device_info_set_pnp_id_vendor_source_id_set(swigCPtr, this, value);
  }

  public short getVendor_source_id() {
    return GemHCIControllerJNI.npe_inc_function_args_bt_device_info_set_pnp_id_vendor_source_id_get(swigCPtr, this);
  }

  public void setVendor_id(int value) {
    GemHCIControllerJNI.npe_inc_function_args_bt_device_info_set_pnp_id_vendor_id_set(swigCPtr, this, value);
  }

  public int getVendor_id() {
    return GemHCIControllerJNI.npe_inc_function_args_bt_device_info_set_pnp_id_vendor_id_get(swigCPtr, this);
  }

  public void setProduct_id(int value) {
    GemHCIControllerJNI.npe_inc_function_args_bt_device_info_set_pnp_id_product_id_set(swigCPtr, this, value);
  }

  public int getProduct_id() {
    return GemHCIControllerJNI.npe_inc_function_args_bt_device_info_set_pnp_id_product_id_get(swigCPtr, this);
  }

  public void setProduct_version(int value) {
    GemHCIControllerJNI.npe_inc_function_args_bt_device_info_set_pnp_id_product_version_set(swigCPtr, this, value);
  }

  public int getProduct_version() {
    return GemHCIControllerJNI.npe_inc_function_args_bt_device_info_set_pnp_id_product_version_get(swigCPtr, this);
  }

  public npe_inc_function_args_bt_device_info_set_pnp_id() {
    this(GemHCIControllerJNI.new_npe_inc_function_args_bt_device_info_set_pnp_id(), true);
  }

}
