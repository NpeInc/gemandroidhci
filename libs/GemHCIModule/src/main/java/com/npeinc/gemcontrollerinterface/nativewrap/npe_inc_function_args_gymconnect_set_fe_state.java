/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.2
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.npeinc.gemcontrollerinterface.nativewrap;

public class npe_inc_function_args_gymconnect_set_fe_state {
  private transient long swigCPtr;
  protected transient boolean swigCMemOwn;

  protected npe_inc_function_args_gymconnect_set_fe_state(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(npe_inc_function_args_gymconnect_set_fe_state obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  @SuppressWarnings("deprecation")
  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        GemHCIControllerJNI.delete_npe_inc_function_args_gymconnect_set_fe_state(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public void setFe_state(wf_gem_hci_gymconnect_fitness_equipment_state_e value) {
    GemHCIControllerJNI.npe_inc_function_args_gymconnect_set_fe_state_fe_state_set(swigCPtr, this, value.swigValue());
  }

  public wf_gem_hci_gymconnect_fitness_equipment_state_e getFe_state() {
    return wf_gem_hci_gymconnect_fitness_equipment_state_e.swigToEnum(GemHCIControllerJNI.npe_inc_function_args_gymconnect_set_fe_state_fe_state_get(swigCPtr, this));
  }

  public npe_inc_function_args_gymconnect_set_fe_state() {
    this(GemHCIControllerJNI.new_npe_inc_function_args_gymconnect_set_fe_state(), true);
  }

}
