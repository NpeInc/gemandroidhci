package com.npeinc.gemcontrollerinterface;

public interface IGemDfuTransferTypeCallback {
    void onGemDfuTransferType(int transfer_type);
}