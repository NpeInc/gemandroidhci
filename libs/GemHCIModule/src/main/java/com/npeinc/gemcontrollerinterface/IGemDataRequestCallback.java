package com.npeinc.gemcontrollerinterface;

public interface IGemDataRequestCallback {
    void onGemDataRequest();
}
