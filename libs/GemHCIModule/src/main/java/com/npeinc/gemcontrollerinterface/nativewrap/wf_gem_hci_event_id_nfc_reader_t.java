/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.2
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.npeinc.gemcontrollerinterface.nativewrap;

public enum wf_gem_hci_event_id_nfc_reader_t {
  WF_GEM_HCI_EVENT_ID_NFC_READER_READ_EVENT(0x01),
  WF_GEM_HCI_EVENT_ID_NFC_TAG_BOARD_READ_EVENT(0x02),
  WF_GEM_HCI_EVENT_ID_NFC_NDEF_READ_EVENT(0x03);

  public final int swigValue() {
    return swigValue;
  }

  public static wf_gem_hci_event_id_nfc_reader_t swigToEnum(int swigValue) {
    wf_gem_hci_event_id_nfc_reader_t[] swigValues = wf_gem_hci_event_id_nfc_reader_t.class.getEnumConstants();
    if (swigValue < swigValues.length && swigValue >= 0 && swigValues[swigValue].swigValue == swigValue)
      return swigValues[swigValue];
    for (wf_gem_hci_event_id_nfc_reader_t swigEnum : swigValues)
      if (swigEnum.swigValue == swigValue)
        return swigEnum;
    throw new IllegalArgumentException("No enum " + wf_gem_hci_event_id_nfc_reader_t.class + " with value " + swigValue);
  }

  @SuppressWarnings("unused")
  private wf_gem_hci_event_id_nfc_reader_t() {
    this.swigValue = SwigNext.next++;
  }

  @SuppressWarnings("unused")
  private wf_gem_hci_event_id_nfc_reader_t(int swigValue) {
    this.swigValue = swigValue;
    SwigNext.next = swigValue+1;
  }

  @SuppressWarnings("unused")
  private wf_gem_hci_event_id_nfc_reader_t(wf_gem_hci_event_id_nfc_reader_t swigEnum) {
    this.swigValue = swigEnum.swigValue;
    SwigNext.next = this.swigValue+1;
  }

  private final int swigValue;

  private static class SwigNext {
    private static int next = 0;
  }
}

