package com.npeinc.gemcontrollerinterface;

public interface IGemEventCallback {
    void onGemEvent(com.npeinc.gemcontrollerinterface.nativewrap.gem_event_t event);
}
