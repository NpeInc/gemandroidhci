/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.2
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.npeinc.gemcontrollerinterface.nativewrap;

public class gem_event_args_t_bt_control_connected {
  private transient long swigCPtr;
  protected transient boolean swigCMemOwn;

  protected gem_event_args_t_bt_control_connected(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(gem_event_args_t_bt_control_connected obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  @SuppressWarnings("deprecation")
  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        GemHCIControllerJNI.delete_gem_event_args_t_bt_control_connected(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public void setConnection_handle(int value) {
    GemHCIControllerJNI.gem_event_args_t_bt_control_connected_connection_handle_set(swigCPtr, this, value);
  }

  public int getConnection_handle() {
    return GemHCIControllerJNI.gem_event_args_t_bt_control_connected_connection_handle_get(swigCPtr, this);
  }

  public void setPeer_address_type(short value) {
    GemHCIControllerJNI.gem_event_args_t_bt_control_connected_peer_address_type_set(swigCPtr, this, value);
  }

  public short getPeer_address_type() {
    return GemHCIControllerJNI.gem_event_args_t_bt_control_connected_peer_address_type_get(swigCPtr, this);
  }

  public void setPeer_address(short[] value) {
    GemHCIControllerJNI.gem_event_args_t_bt_control_connected_peer_address_set(swigCPtr, this, value);
  }

  public short[] getPeer_address() {
    return GemHCIControllerJNI.gem_event_args_t_bt_control_connected_peer_address_get(swigCPtr, this);
  }

  public void setDevice_type(short value) {
    GemHCIControllerJNI.gem_event_args_t_bt_control_connected_device_type_set(swigCPtr, this, value);
  }

  public short getDevice_type() {
    return GemHCIControllerJNI.gem_event_args_t_bt_control_connected_device_type_get(swigCPtr, this);
  }

  public gem_event_args_t_bt_control_connected() {
    this(GemHCIControllerJNI.new_gem_event_args_t_bt_control_connected(), true);
  }

}
