package com.npeinc.gemcontrollerinterface;

// Port of npe_gem_hci_library_response_strings.h

import android.util.Log;

import java.util.Locale;

public class GemResponseCodeParser {

    private static final String TAG = "GEM";

    private static final int NPE_GEM_HCI_COMMON_ERROR_BASE = 200;
    private static final int NPE_GEM_HCI_COMMON_UNKNOWN_ERROR_BASE = 254;

    public static void NPE_GEM_HCI_LIB_PRINT_IF_ERROR(int response_code, String message) {
        if (response_code != 0)
        {
            String errorMessage = String.format(Locale.US, "Response Code %d %s", response_code, message);
            Log.e(TAG, errorMessage);
            System.out.println(errorMessage);
        }
    }

    //TODO!! - check that index fits into specific array, and other array boundaries.
    private static String NPE_GEM_HCI_LIB_PROCESS_ERROR_CODE(String[] specific_error_array, int error_code) {

        return error_code < 0 ? String.valueOf(error_code)
                : error_code < NPE_GEM_HCI_COMMON_ERROR_BASE ? specific_error_array[error_code]
                : error_code < NPE_GEM_HCI_COMMON_UNKNOWN_ERROR_BASE ? npe_hci_common_error_code_string[error_code - NPE_GEM_HCI_COMMON_ERROR_BASE]
                : npe_hci_common_unknown_error_code_string[error_code - NPE_GEM_HCI_COMMON_UNKNOWN_ERROR_BASE];

    }

    public static String NPE_GEM_HCI_LIB_HW_SET_PINS_ERROR_CODE_STR(int error_code) {
        return NPE_GEM_HCI_LIB_PROCESS_ERROR_CODE(npe_hci_hardware_set_pins_error_code_string, error_code);
    }

    public static String NPE_GEM_HCI_LIB_GET_BT_CONTRL_ADV_START_ERROR_CODE_STR(int error_code) {
        return NPE_GEM_HCI_LIB_PROCESS_ERROR_CODE(npe_hci_bluetooth_control_start_advertising_response_error_code_string,
                error_code);
    }

    public static String NPE_GEM_HCI_LIB_GET_BT_CONTRL_ADV_STOP_ERROR_CODE_STR(int error_code)  {
        return NPE_GEM_HCI_LIB_PROCESS_ERROR_CODE(npe_hci_bluetooth_control_stop_advertising_response_error_code_string, error_code);
    }

    public static String NPE_GEM_HCI_LIB_NFC_ENABLE_DISABLE_ERROR_CODE_STR(int error_code)  {
        return NPE_GEM_HCI_LIB_PROCESS_ERROR_CODE(npe_hci_nfc_enable_disable_response_error_code_string, error_code);
    }

    public static String NPE_GEM_HCI_LIB_GET_BT_CONFIG_SET_DEVICE_NAME_ERROR_CODE_STR(int error_code)  {
        return NPE_GEM_HCI_LIB_PROCESS_ERROR_CODE(npe_hci_string_response_error_code_string, error_code);
    }

    public static String NPE_GEM_HCI_LIB_GET_BT_INFO_SET_MANUFACTURER_NAME_ERROR_CODE_STR(int error_code)  {
        return NPE_GEM_HCI_LIB_PROCESS_ERROR_CODE(npe_hci_string_response_error_code_string, error_code);
    }

    public static String NPE_GEM_HCI_LIB_GET_BT_INFO_SET_MODEL_NUMBER_ERROR_CODE_STR(int error_code)  {
        return NPE_GEM_HCI_LIB_PROCESS_ERROR_CODE(npe_hci_generic_response_error_code_string, error_code);
    }

    public static String NPE_GEM_HCI_LIB_GET_BT_INFO_SET_SERIAL_NUMBER_ERROR_CODE_STR(int error_code)  {
        return NPE_GEM_HCI_LIB_PROCESS_ERROR_CODE(npe_hci_string_response_error_code_string, error_code);
    }

    public static String NPE_GEM_HCI_LIB_GET_BT_INFO_SET_FIRMWARE_REVISION_ERROR_CODE_STR(int error_code)  {
        return NPE_GEM_HCI_LIB_PROCESS_ERROR_CODE(npe_hci_string_response_error_code_string, error_code);
    }

    public static String NPE_GEM_HCI_LIB_GET_BT_INFO_SET_HARDWARE_REVISION_ERROR_CODE_STR(int error_code)  {
        return NPE_GEM_HCI_LIB_PROCESS_ERROR_CODE(npe_hci_string_response_error_code_string, error_code);
    }

    public static String NPE_GEM_HCI_LIB_GET_BT_INFO_SET_BATTERY_SERVICE_INCLUDED_ERROR_CODE_STR(int error_code)  {
        return NPE_GEM_HCI_LIB_PROCESS_ERROR_CODE(npe_hci_generic_response_error_code_string, error_code);
    }
    public static String NPE_GEM_HCI_LIB_GET_BT_INFO_SET_PNP_ID_ERROR_CODE_STR(int error_code)  {
        return NPE_GEM_HCI_LIB_PROCESS_ERROR_CODE(npe_hci_generic_response_error_code_string, error_code);
    }

    public static String NPE_GEM_HCI_LIB_GET_ANT_CONFIG_SET_HARDWARE_VERSION_ERROR_CODE_STR(int error_code)  {
        return NPE_GEM_HCI_LIB_PROCESS_ERROR_CODE(npe_hci_generic_response_error_code_string, error_code);
    }

    public static String NPE_GEM_HCI_LIB_GET_ANT_CONFIG_SET_MODEL_NUMBER_ERROR_CODE_STR(int error_code)  {
        return NPE_GEM_HCI_LIB_PROCESS_ERROR_CODE(npe_hci_generic_response_error_code_string, error_code);
    }

    public static String NPE_GEM_HCI_LIB_GET_ANT_CONFIG_SET_SOFTWARE_VERSION_ERROR_CODE_STR(int error_code)  {
        return NPE_GEM_HCI_LIB_PROCESS_ERROR_CODE(npe_hci_generic_response_error_code_string, error_code);
    }

    public static String NPE_GEM_HCI_LIB_GET_ANT_CONFIG_SET_SERIAL_NUMBER_ERROR_CODE_STR(int error_code)  {
        return NPE_GEM_HCI_LIB_PROCESS_ERROR_CODE(npe_hci_generic_response_error_code_string, error_code);
    }

    public static String NPE_GEM_HCI_LIB_GET_GYMCONNECT_SET_FITNESS_EQUIPMENT_TYPE_ERROR_CODE_STR(int error_code)  {
        return NPE_GEM_HCI_LIB_PROCESS_ERROR_CODE(npe_hci_generic_response_error_code_string, error_code);
    }

    public static String NPE_GEM_HCI_LIB_GET_GYMCONNECT_SET_FITNESS_EQUIPMENT_STATE_ERROR_CODE_STR(int error_code)  {
        return NPE_GEM_HCI_LIB_PROCESS_ERROR_CODE(npe_hci_generic_response_error_code_string, error_code);
    }

    public static String NPE_GEM_HCI_LIB_GET_GYMCONNECT_SET_SUPPORTED_EQUIPMENT_CONTROL_ERROR_CODE_STR(int error_code)  {
        return NPE_GEM_HCI_LIB_PROCESS_ERROR_CODE(npe_hci_gymconnect_set_equipment_control_error_code_string, error_code);
    }

    public static String NPE_GEM_HCI_LIB_GET_GYMCONNECT_WORKOUT_DATA_UPDATE_ERROR_CODE_STR(int error_code)  {
        return NPE_GEM_HCI_LIB_PROCESS_ERROR_CODE(npe_hci_generic_response_error_code_string, error_code);
    }

    public static String NPE_GEM_HCI_LIB_ANT_RECEIVER_START_DISCOVERY_ERROR_CODE_STR(int error_code)  {
        return NPE_GEM_HCI_LIB_PROCESS_ERROR_CODE(npe_hci_ant_receiver_start_discovery_error_code_string, error_code);
    }

    public static String NPE_GEM_HCI_LIB_NFC_READER_ERROR_CODE_STR(int error_code)  {
        return NPE_GEM_HCI_LIB_PROCESS_ERROR_CODE(npe_hci_nfc_reader_error_code_string, error_code);
    }


    private static final String npe_hci_generic_response_error_code_string[] = {
            "Success"
    };

    private static final String npe_hci_string_response_error_code_string[] = {
            "Success",
            "Full Device Name will not fit, it has been truncated"
    };


    private static final String npe_hci_hardware_set_pins_error_code_string[] = {
            "Success",
            "One or more invalid pin numbers were supplied",
            "One or more invalid Pin IO Modes were supplied"
    };

    private static final String npe_hci_bluetooth_control_start_advertising_response_error_code_string[] = {
            "Success",
            "Error - Already Advertising",
            "Error - Already Connected"
    };

    private static final String npe_hci_bluetooth_control_stop_advertising_response_error_code_string[] = {
            "Success",
            "Error - Was Not Advertising",
            "Error - Already Connected"
    };

    private static final String npe_hci_nfc_enable_disable_response_error_code_string[] = {
            "Success",
            "Unsupported by device"
    };

    private static final String npe_hci_common_error_code_string[] = {
            "Invalid Data Length",          // 200
            "Invalid Parameter Value",      // 201
            "Malformed UTF-8 string",       // 202
            "Incorrect State",              // 203
            "Not Supported",                // 204
    };


    private static final String npe_hci_common_unknown_error_code_string[] = {
            "Unknown Message Type",         // 254
            "Unknown Error"                 // 255
    };

    private static final String npe_hci_gymconnect_set_equipment_control_error_code_string[] = {
            "Success",
            "Error - BLE Advertising",
            "Error - BLE Connection Established"
    };

    private static final String npe_hci_ant_receiver_start_discovery_error_code_string[] = {
            "Success",
            "Invalid/Unsupported ANT+ Receiver Profile specified",
            "UNKNOWN 2",
            "UNKNOWN 3",
            "UNKNOWN 4",
            "UNKNOWN 5",
            "UNKNOWN 6",
            "UNKNOWN 7",
            "UNKNOWN 8",
            "UNKNOWN 9",
            "UNKNOWN 10",
            "UNKNOWN 11",
            "UNKNOWN 12",
            "UNKNOWN 13",
            "UNKNOWN 14",
            "UNKNOWN 15",
            "UNKNOWN 16",
            "UNKNOWN 17",
            "UNKNOWN 18",
            "UNKNOWN 19",
            "Unable to begin discovery process"
    };

    private static final String npe_hci_nfc_reader_error_code_string[] = {
            "Success",
            "Unsupported by device"
    };

}
