/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.2
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.npeinc.gemcontrollerinterface.nativewrap;

public class npe_inc_function_args_bt_config_set_device_name {
  private transient long swigCPtr;
  protected transient boolean swigCMemOwn;

  protected npe_inc_function_args_bt_config_set_device_name(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(npe_inc_function_args_bt_config_set_device_name obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  @SuppressWarnings("deprecation")
  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        GemHCIControllerJNI.delete_npe_inc_function_args_bt_config_set_device_name(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public void setBluetooth_name(String value) {
    GemHCIControllerJNI.npe_inc_function_args_bt_config_set_device_name_bluetooth_name_set(swigCPtr, this, value);
  }

  public String getBluetooth_name() {
    return GemHCIControllerJNI.npe_inc_function_args_bt_config_set_device_name_bluetooth_name_get(swigCPtr, this);
  }

  public npe_inc_function_args_bt_config_set_device_name() {
    this(GemHCIControllerJNI.new_npe_inc_function_args_bt_config_set_device_name(), true);
  }

}
