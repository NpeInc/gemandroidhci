/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.2
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.npeinc.gemcontrollerinterface.nativewrap;

public enum wf_gem_hci_event_id_gym_connect_e {
  WF_GEM_HCI_EVENT_ID_GYM_CONNECT_HEART_RATE_VALUE_RECEIVED(0x01),
  WF_GEM_HCI_EVENT_ID_GYM_CONNECT_CADENCE_VALUE_RECEIVED(0x02),
  WF_GEM_HCI_EVENT_ID_GYM_CONNECT_USER_INFORMATION_RECEIVED(0x03),
  WF_GEM_HCI_EVENT_ID_GYM_CONNECT_CALORIE_DATA_RECEIVED(0x04),
  WF_GEM_HCI_EVENT_ID_GYM_CONNECT_REQUEST_SUPPORTED_UPLOAD_ITEM_TYPES_EVENT(0x40),
  WF_GEM_HCI_EVENT_ID_GYM_CONNECT_BEGIN_UPLOAD_EVENT(0x41),
  WF_GEM_HCI_EVENT_ID_GYM_CONNECT_UPLOAD_CHUNK_EVENT(0x42),
  WF_GEM_HCI_EVENT_ID_GYM_CONNECT_FINISH_UPLOAD_EVENT(0x43),
  WF_GEM_HCI_EVENT_ID_GYM_CONNECT_CANCEL_UPLOAD_EVENT(0x44),
  WF_GEM_HCI_EVENT_ID_GYM_CONNECT_REQUEST_UPLOAD_PROCESSING_AND_ISSUES_EVENT(0x45),
  WF_GEM_HCI_EVENT_ID_GYM_CONNECT_EQUIPMENT_CONTROL_EVENT(0x51);

  public final int swigValue() {
    return swigValue;
  }

  public static wf_gem_hci_event_id_gym_connect_e swigToEnum(int swigValue) {
    wf_gem_hci_event_id_gym_connect_e[] swigValues = wf_gem_hci_event_id_gym_connect_e.class.getEnumConstants();
    if (swigValue < swigValues.length && swigValue >= 0 && swigValues[swigValue].swigValue == swigValue)
      return swigValues[swigValue];
    for (wf_gem_hci_event_id_gym_connect_e swigEnum : swigValues)
      if (swigEnum.swigValue == swigValue)
        return swigEnum;
    throw new IllegalArgumentException("No enum " + wf_gem_hci_event_id_gym_connect_e.class + " with value " + swigValue);
  }

  @SuppressWarnings("unused")
  private wf_gem_hci_event_id_gym_connect_e() {
    this.swigValue = SwigNext.next++;
  }

  @SuppressWarnings("unused")
  private wf_gem_hci_event_id_gym_connect_e(int swigValue) {
    this.swigValue = swigValue;
    SwigNext.next = swigValue+1;
  }

  @SuppressWarnings("unused")
  private wf_gem_hci_event_id_gym_connect_e(wf_gem_hci_event_id_gym_connect_e swigEnum) {
    this.swigValue = swigEnum.swigValue;
    SwigNext.next = this.swigValue+1;
  }

  private final int swigValue;

  private static class SwigNext {
    private static int next = 0;
  }
}

