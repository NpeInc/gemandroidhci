/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.2
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.npeinc.gemcontrollerinterface.nativewrap;

public enum wf_gem_hci_command_id_gym_connect_e {
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_GET_FE_TYPE(0x01),
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_TYPE(0x02),
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_GET_FE_STATE(0x03),
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_STATE(0x04),
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_GET_FE_WORKOUT_PROG_NAME(0x05),
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_WORKOUT_PROG_NAME(0x06),
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_UPDATE_WORKOUT_DATA(0x23),
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_GET_ADDITIONAL_OPTIONS(0x24),
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_ADDITIONAL_OPTIONS(0x25),
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_NOTIFY_SUPPORTED_UPLOAD_ITEM_TYPES_MESSAGE(0x40),
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_NOTIFY_BEGIN_UPLOAD_RECEIVED_MESSAGE(0x41),
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_NOTIFY_UPLOAD_CHUNK_RECEIVED_MESSAGE(0x42),
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_NOTIFY_FINISH_UPLOAD_RECEIVED_MESSAGE(0x43),
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_NOTIFY_CANCEL_UPLOAD_RECEIVED_MESSAGE(0x44),
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_NOTIFY_UPLOAD_ERROR_MESSAGE(0x45),
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_NOTIFY_NOTIFY_UPLOAD_PROCESSING_AND_ISSUES_MESSAGE(0x46),
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_GET_FE_CONTROL_FEATURES(0x4F),
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_CONTROL_FEATURES(0x50),
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_NOTIFY_EQUIPMENT_CONTROL_RECEIVED_MESSAGE(0x51),
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_SPEED_RANGE(0x52),
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_GET_FE_SPEED_RANGE(0x53),
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_INCLINE_CONTROL_RANGE(0x54),
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_GET_FE_INCLINE_CONTROL_RANGE(0x55),
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_RESISTANCE_CONTROL_RANGE(0x56),
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_GET_FE_RESISTANCE_CONTROL_RANGE(0x57),
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_TARGET_POWER_CONTROL_RANGE(0x58),
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_GET_FE_TARGET_POWER_CONTROL_RANGE(0x59),
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_SET_FE_TARGET_HEART_RATE_CONTROL_RANGE(0x5A),
  WF_GEM_HCI_COMMAND_ID_GYM_CONNECT_GET_FE_TARGET_HEART_RATE_CONTROL_RANGE(0x5B);

  public final int swigValue() {
    return swigValue;
  }

  public static wf_gem_hci_command_id_gym_connect_e swigToEnum(int swigValue) {
    wf_gem_hci_command_id_gym_connect_e[] swigValues = wf_gem_hci_command_id_gym_connect_e.class.getEnumConstants();
    if (swigValue < swigValues.length && swigValue >= 0 && swigValues[swigValue].swigValue == swigValue)
      return swigValues[swigValue];
    for (wf_gem_hci_command_id_gym_connect_e swigEnum : swigValues)
      if (swigEnum.swigValue == swigValue)
        return swigEnum;
    throw new IllegalArgumentException("No enum " + wf_gem_hci_command_id_gym_connect_e.class + " with value " + swigValue);
  }

  @SuppressWarnings("unused")
  private wf_gem_hci_command_id_gym_connect_e() {
    this.swigValue = SwigNext.next++;
  }

  @SuppressWarnings("unused")
  private wf_gem_hci_command_id_gym_connect_e(int swigValue) {
    this.swigValue = swigValue;
    SwigNext.next = swigValue+1;
  }

  @SuppressWarnings("unused")
  private wf_gem_hci_command_id_gym_connect_e(wf_gem_hci_command_id_gym_connect_e swigEnum) {
    this.swigValue = swigEnum.swigValue;
    SwigNext.next = this.swigValue+1;
  }

  private final int swigValue;

  private static class SwigNext {
    private static int next = 0;
  }
}

