/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.2
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.npeinc.gemcontrollerinterface.nativewrap;

public class npe_inc_function_args_ant_receiver_request_battery_status {
  private transient long swigCPtr;
  protected transient boolean swigCMemOwn;

  protected npe_inc_function_args_ant_receiver_request_battery_status(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(npe_inc_function_args_ant_receiver_request_battery_status obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  @SuppressWarnings("deprecation")
  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        GemHCIControllerJNI.delete_npe_inc_function_args_ant_receiver_request_battery_status(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public void setAnt_plus_device_profile(int value) {
    GemHCIControllerJNI.npe_inc_function_args_ant_receiver_request_battery_status_ant_plus_device_profile_set(swigCPtr, this, value);
  }

  public int getAnt_plus_device_profile() {
    return GemHCIControllerJNI.npe_inc_function_args_ant_receiver_request_battery_status_ant_plus_device_profile_get(swigCPtr, this);
  }

  public void setData(wf_gem_hci_ant_receiver_battery_status_data_t value) {
    GemHCIControllerJNI.npe_inc_function_args_ant_receiver_request_battery_status_data_set(swigCPtr, this, wf_gem_hci_ant_receiver_battery_status_data_t.getCPtr(value), value);
  }

  public wf_gem_hci_ant_receiver_battery_status_data_t getData() {
    long cPtr = GemHCIControllerJNI.npe_inc_function_args_ant_receiver_request_battery_status_data_get(swigCPtr, this);
    return (cPtr == 0) ? null : new wf_gem_hci_ant_receiver_battery_status_data_t(cPtr, false);
  }

  public npe_inc_function_args_ant_receiver_request_battery_status() {
    this(GemHCIControllerJNI.new_npe_inc_function_args_ant_receiver_request_battery_status(), true);
  }

}
