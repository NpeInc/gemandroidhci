package com.npeinc.gemcontrollerinterface;

public interface IGemDfuProgressCallback {
    void onGemDfuProgress(int progress);
}