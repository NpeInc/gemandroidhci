/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.2
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package com.npeinc.gemcontrollerinterface.nativewrap;

public class npe_inc_function_args_ant_receiver_connect_device {
  private transient long swigCPtr;
  protected transient boolean swigCMemOwn;

  protected npe_inc_function_args_ant_receiver_connect_device(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(npe_inc_function_args_ant_receiver_connect_device obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  @SuppressWarnings("deprecation")
  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        GemHCIControllerJNI.delete_npe_inc_function_args_ant_receiver_connect_device(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public void setAnt_plus_device_profile(int value) {
    GemHCIControllerJNI.npe_inc_function_args_ant_receiver_connect_device_ant_plus_device_profile_set(swigCPtr, this, value);
  }

  public int getAnt_plus_device_profile() {
    return GemHCIControllerJNI.npe_inc_function_args_ant_receiver_connect_device_ant_plus_device_profile_get(swigCPtr, this);
  }

  public void setDevice_number(long value) {
    GemHCIControllerJNI.npe_inc_function_args_ant_receiver_connect_device_device_number_set(swigCPtr, this, value);
  }

  public long getDevice_number() {
    return GemHCIControllerJNI.npe_inc_function_args_ant_receiver_connect_device_device_number_get(swigCPtr, this);
  }

  public void setProximity_bin(short value) {
    GemHCIControllerJNI.npe_inc_function_args_ant_receiver_connect_device_proximity_bin_set(swigCPtr, this, value);
  }

  public short getProximity_bin() {
    return GemHCIControllerJNI.npe_inc_function_args_ant_receiver_connect_device_proximity_bin_get(swigCPtr, this);
  }

  public void setConnection_timeout(short value) {
    GemHCIControllerJNI.npe_inc_function_args_ant_receiver_connect_device_connection_timeout_set(swigCPtr, this, value);
  }

  public short getConnection_timeout() {
    return GemHCIControllerJNI.npe_inc_function_args_ant_receiver_connect_device_connection_timeout_get(swigCPtr, this);
  }

  public npe_inc_function_args_ant_receiver_connect_device() {
    this(GemHCIControllerJNI.new_npe_inc_function_args_ant_receiver_connect_device(), true);
  }

}
